//
//  SheetItem.h
//  List Assist
//
//  Created by Admin on 11/22/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SheetItem : NSManagedObject

@property (nonatomic, retain) NSString * additiondesc;
@property (nonatomic, retain) NSString * buynowprice;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * item;
@property (nonatomic, retain) NSString * minstartingbid;
@property (nonatomic, retain) NSString * photos;
@property (nonatomic, retain) NSString * quantity;
@property (nonatomic, retain) NSString * reserveprice;
@property (nonatomic, retain) NSString * seller;
@property (nonatomic, retain) NSString * taxable;
@property (nonatomic, retain) NSString * thumbnails;
@property (nonatomic, retain) NSString * sheetname;

@end
