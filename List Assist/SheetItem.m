//
//  SheetItem.m
//  List Assist
//
//  Created by Admin on 11/22/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "SheetItem.h"


@implementation SheetItem

@dynamic additiondesc;
@dynamic buynowprice;
@dynamic category;
@dynamic desc;
@dynamic item;
@dynamic minstartingbid;
@dynamic photos;
@dynamic quantity;
@dynamic reserveprice;
@dynamic seller;
@dynamic taxable;
@dynamic thumbnails;
@dynamic sheetname;

@end
