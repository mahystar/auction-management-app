//
//  STDataInfo.m
//  CarPool
//
//  Created by RiKS on 10/2/13.
//  Copyright (c) 2013 RiKS. All rights reserved.
//

#import "Global.h"
#import "STDataInfo.h"

@implementation Global

+ (int) MAX_PICTURES
{
    return 5;
}

+ (NSMutableArray*) csvHeaders
{
    return [[NSMutableArray alloc] initWithObjects:@"item", @"Category", @"Description", @"Additional Description", @"Photos", @"Taxable", @"Minimum Starting Bid", @"Thumbnails", @"Reserve Price", @"Quantity", @"Seller", @"Buy Now Price", nil];
    
}

+ (NSString*) worksheetFileName
{
    return @"Worksheets.txt";
}

+ (NSString*) appDataDir
{
    return @"Auction_Data";
}

+ (NSString*) sheetFilesDir
{
    return @"Worksheets";
}

+ (NSString*) photoFilesDir
{
    return @"Photos";
}

+ (NSString*) exportFilesDir
{
    return @"Export";
}

+ (NSString*) originDir
{
    return @"Origin";
}

+ (NSString*) originTempDir
{
    return @"Origin_Temp";
}

+ (NSString*) resultPhotoDir
{
    return @"Auction_Photo";
}

+ (NSString*) passWord
{
    return @"BiD!585AsSeTs!";
}

- (NSString*) getImageFileName:(NSString*)strId index:(int)index tumb:(BOOL)bThumb
{
    NSString *strRet = @"";
    NSString *indexStr = @"";
    
    if (index != -1)
    {
        //indexStr = String.valueOf((char) ('A' + index));
        int hexA = 0x41; // Hex: A
        indexStr = [NSString stringWithFormat:@"%c", (char)(hexA + index)];
    }
    
    //strRet = strId + indexStr + ((bThumb) ? "t" : "");
    strRet = [NSString stringWithFormat:@"%@%@%@", strId, indexStr, ((bThumb) ? @"t" : @"")];
    
    return strRet;
}

- (BOOL) checkIDVerify:(NSString*)newId sheetList:(NSMutableArray*)sheetList sheetId:(int)sheetId
{
    if (sheetList == nil)
        return true;
    
    BOOL bValid = true;
    //for (int i = 0; i < sheetList.size(); i++)
    for (int i = 0; i < [sheetList count]; i++)
    {
        //if (sheetList.get(i).item.equals(newId) && i != sheetId)
        STSheetItem *sheetitem = [sheetList objectAtIndex:i];
        if ([sheetitem.item isEqualToString:newId] && i != sheetId)
        {
            bValid = false;
            break;
        }
    }
    
    return bValid;
}

- (int) GetItemFromItemName:(NSString*)itemNumber sheetList:(NSMutableArray*)sheetList
{
    int retId = -1;
    
    if (sheetList == nil)
        return retId;
    
    //for (int i = 0; i < sheetList.size(); i++)
    for (int i = 0; i < [sheetList count]; i++)
    {
        //if (sheetList.get(i).item.equals(itemNumber))
        STSheetItem *sheetitem = [sheetList objectAtIndex:i];
        if ([sheetitem.item isEqualToString:itemNumber])
        {
            retId = i;
            break;
        }
    }
    
    return retId;
}

@end















