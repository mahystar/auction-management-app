//
//  STDataInfo.h
//  CarPool
//
//  Created by RiKS on 10/2/13.
//  Copyright (c) 2013 RiKS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#import "STDataInfo.h"

@interface DataMgr : NSObject

+ (NSMutableArray*) getWorksheets;
+ (void) setWorksheets:(NSMutableArray*)worksheetList;
+ (NSMutableArray*) getSheetItems:(NSString*)filename;
+ (void) setSheetItems:(NSMutableArray*)sheetItems filename:(NSString*)filename;
+ (void) ImportWorkSheet:(STWorkSheet*)worksheetList sheetName:(NSString*)sheetName sheetFile:(NSString*)sheetfile;
+ (NSString*)remCommaFmData:(NSString*)record;
+ (void)ExportWorkSheet:(NSMutableArray*)sheetList sheetName:(NSString*)sheetName;
+ (void)RenameWorkSheet:(NSString*)originName newName:(NSString*)newName;
+ (void)EncodeString:(NSString*)originStr;
+ (void)DecodeString:(NSString*)destStr;

@end
