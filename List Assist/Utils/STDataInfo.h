//
//  STDataInfo.h
//  CarPool
//
//  Created by RiKS on 10/2/13.
//  Copyright (c) 2013 RiKS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface STDataInfo : NSObject

@end

@interface STWorkSheet : NSObject
{
	NSString *sheetName;
	NSString *description;
    
    NSMutableArray *sheetitems;
}

@property (nonatomic, retain) NSString *sheetName;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, readwrite) NSMutableArray *sheetitems;

- (id)initSheetWithName:(NSString *)sheetName descrption:(NSString *)desc;

@end


@interface STSheetItem : NSObject
{
    NSString *item;
    NSString *category;
    NSString *desc;
    NSString *additiondesc;
    NSString *photos;
    NSString *taxable;
    NSString *minstartingbid;
    NSString *thumbnails;
    NSString *reserveprice;
    NSString *quantity;
    NSString *seller;
    NSString *buynowprice;
}

@property (nonatomic, readwrite) NSString *item;
@property (nonatomic, readwrite) NSString *category;
@property (nonatomic, readwrite) NSString *description;
@property (nonatomic, readwrite) NSString *adddescription;
@property (nonatomic, readwrite) NSString *photos;
@property (nonatomic, readwrite) NSString *taxable;
@property (nonatomic, readwrite) NSString *minstartingbid;
@property (nonatomic, readwrite) NSString *thumbnails;
@property (nonatomic, readwrite) NSString *reserveprice;
@property (nonatomic, readwrite) NSString *quantity;
@property (nonatomic, readwrite) NSString *seller;
@property (nonatomic, readwrite) NSString *buynowprice;

@end
