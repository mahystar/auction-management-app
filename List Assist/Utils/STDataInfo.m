//
//  STDataInfo.m
//  CarPool
//
//  Created by RiKS on 10/2/13.
//  Copyright (c) 2013 RiKS. All rights reserved.
//

#import "STDataInfo.h"

@implementation STDataInfo

@end

@implementation STWorkSheet

@synthesize sheetName;
@synthesize description;

@synthesize sheetitems;

- (id)initSheetWithName:(NSString *)sheetName descrption:(NSString *)desc {
    
    if ((self = [super init])) {
        self.description = desc;
        self.sheetName = sheetName;
    }
    return self;
    
}

- (void) dealloc {
    self.sheetName = nil;
//    [super dealloc];
}

- (id)init {
    
    if ((self = [super init])) {
        self.sheetitems = [[NSMutableArray alloc] init];
    }
    return self;
    
}
@end


@implementation STSheetItem

@synthesize item;
@synthesize category;
@synthesize description;
@synthesize adddescription;
@synthesize photos;
@synthesize taxable;
@synthesize minstartingbid;
@synthesize thumbnails;
@synthesize reserveprice;
@synthesize quantity;
@synthesize seller;
@synthesize buynowprice;

@end
