//
//  STDataInfo.h
//  CarPool
//
//  Created by RiKS on 10/2/13.
//  Copyright (c) 2013 RiKS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface Global : NSObject

+ (int) MAX_PICTURES;
+ (NSMutableArray*) csvHeaders;
+ (NSString*) worksheetFileName;
+ (NSString*) appDataDir;
+ (NSString*) sheetFilesDir;
+ (NSString*) photoFilesDir;
+ (NSString*) exportFilesDir;
+ (NSString*) originDir;
+ (NSString*) originTempDir;
+ (NSString*) resultPhotoDir;
+ (NSString*) passWord;


- (NSString*) getImageFileName:(NSString*)strId index:(int)index tumb:(BOOL)bThumb;
- (BOOL) checkIDVerify:(NSString*)newId sheetList:(NSMutableArray*)sheetList sheetId:(int)sheetId;
- (int) GetItemFromItemName:(NSString*)itemNumber sheetList:(NSMutableArray*)sheetList;

@end
