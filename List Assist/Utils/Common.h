//
//  Common.h
//  4S-C
//
//  Created by AnH on 4/25/14.
//  Copyright (c) 2013 PIC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


#define MOVE_FROM_RIGHT     CATransition *animation = [CATransition animation]; \
                            [animation setDuration:0.3]; \
                            [animation setType:kCATransitionPush]; \
                            [animation setSubtype:kCATransitionFromRight]; \
                            [animation setTimingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut]]; \
                            [[self.view.superview layer] addAnimation:animation forKey:@"SwitchToView"];

#define MOVE_FROM_LEFT      CATransition *animation = [CATransition animation]; \
                            [animation setDuration:0.3]; \
                            [animation setType:kCATransitionPush]; \
                            [animation setSubtype:kCATransitionFromLeft]; \
                            [animation setTimingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut]]; \
                            [[self.view.superview layer] addAnimation:animation forKey:@"SwitchToView"];

#define SHOW_VIEW(ctrl)     MOVE_FROM_RIGHT \
                            [self presentViewController:ctrl animated:NO completion:nil];

#define BACK_VIEW           MOVE_FROM_LEFT \
                            [self dismissViewControllerAnimated:NO completion:nil];

#define TEST_NETWORK_RETURN if ([CommManager hasConnectivity] == NO) { \
                                [DejalActivityView removeView]; \
                                [Common showToastWithMessage:@"No network link." parent:self]; \
                                return; \
                            }

#define BACKGROUND_TEST_NETWORK_RETURN    if ([CommManager hasConnectivity] == NO) { \
                                                return; \
                                            }


typedef NS_ENUM(NSInteger, DEVICE_KIND) {
    IPHONE4= 1,
    IPHONE5,
    IPAD,
};

@interface Common : NSObject {
}

+ (void) setDeviceToken : (NSString*)newDeviceToken;
+ (NSString*) deviceToken;

+ (void) setUserId : (NSString*)newUserId;
+ (NSString*) userId;

+ (NSString*) getCurTime : (NSString*)fmt;

+ (NSInteger) MAXLENGTH;

+ (NSInteger) phoneType;

+ (NSData*)base64forString:(NSString*)theString;

+ (NSString *) appNameAndVersionNumberDisplayString;

+ (void) showToastWithMessage:(NSString*) msg parent:(UIViewController *) parent;

+ (NSString * ) getMainStoryBoardName;

+ (NSString *) getMainTitle;

+ (BOOL) readBoolEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(BOOL)defaults;
+ (float) readFloatEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(float)defaults;
+ (int) readIntEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(int)defaults;
+ (double) readDoubleEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(double)defaults;
+ (NSString *) readEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSString *)defaults;

@property (nonatomic, readwrite) int pageIndex;


@end
