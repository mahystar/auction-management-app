//
//  ViewController.m
//  List Assist
//
//  Created by Admin on 9/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "LicenseViewController.h"

#import "Common.h"

BOOL bAgreeFlag;


@interface LicenseViewController ()

@end

@implementation LicenseViewController

@synthesize btnHere;
@synthesize btnAgreeCheck;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:true];
    
    NSUserDefaults *config = [NSUserDefaults standardUserDefaults];
    if ([Common readBoolEntry:config key:@"Agree" defaults:NO])
    {
        [self showWorkSheetListView];
    }

    bAgreeFlag = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)OnHereClick:(id)sender {
    
    NSString *strTermsCondition = @"By using this application: User agrees that they are contracted with Bid-Assets to sell property on the Bid-Assets.com auction portal and no others. User agrees to not share this app with anyone outside of their organization who is not contracted with Bid-Assets.com User agrees not to tamper or copy any part of the application Code, User Interface, Java, Photo Watermarks, for their own use or benefit to others or themselves. User agrees to hold harmless Bid-Assets, their owners, and/or employees for any non-performance of this app. The User agrees that they are using this app at their own risk.            User agrees to hold harmless Bid-Assets, their owners, and/or employees for the content of items and photos uploaded from the application.\n LIMITATION OF LIABILITY \nIn no event will Bid-Assets.com or it\'s employees be liable for any direct, indirect, special, punitive, exemplary or consequential losses or damages of whatsoever kind arising out of your use or access to the Application, including loss of profit or the like whether or not in the contemplation of the parties, whether based on breach of contract, tort (including negligence), product liability or otherwise.  Bid-Assets is not liable to you for any damage or alteration to your equipment including but not limited to computer equipment, handheld device or mobile telephones as a result of the installation or use of the Application.Nothing in these Terms shall exclude or limit Bid-Asset\'s liability for death or personal injury caused by negligence or for fraud or fraudulent misrepresentation or any other liability which cannot be excluded or limited under applicable law.\n DISCLAIMER OF WARRANTIES \nTo the maximum extent permitted by law, Bid-Assets hereby disclaims all implied warranties with regard to this app. The Application and software are provided \"as is\" and \"as available\" without warranty of any kind.";
                
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Terms & Conditions"
                                                     message:strTermsCondition
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles: nil];
    [alert show];
}

- (IBAction)OnAgreeClick:(id)sender {
    
    bAgreeFlag = !bAgreeFlag;

    btnAgreeCheck.selected = bAgreeFlag;
}

- (IBAction)OnProceedClick:(id)sender {
    
    if (bAgreeFlag)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:((bAgreeFlag) ? @"YES" : @"NO") forKey:@"Agree"];
        [defaults synchronize];
        
        [self showWorkSheetListView];
    }
    else
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Alert"
                                                         message:@"Please Agree to Terms and Condition before proceeding"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }
}

- (void)showWorkSheetListView
{
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    
    UIViewController *worksheetlistViewCtrl=[board instantiateViewControllerWithIdentifier:@"WorkSheetList"];
    
    [self.navigationController pushViewController:worksheetlistViewCtrl animated:YES];
}

@end
