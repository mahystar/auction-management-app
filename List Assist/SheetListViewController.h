//
//  SheetListViewController.h
//  List Assist
//
//  Created by Admin on 9/19/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STDataInfo.h"
#import "SheetEditViewController.h"
#import "Sheet.h"
#import "AppDelegate.h"
#import "HelpViewController.h"
#import "GRRequestsManager.h"
#import "GRRequestProtocol.h"
#import "MBProgressHUD.h"

@class SheetListViewController;

@protocol SheetListViewDelegate

- (void)OnChangedClose:(SheetListViewController *)sheetlistVC;
- (void)OnClose:(SheetListViewController *)sheetlistVC;

@end

@interface SheetListViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource,UITableViewDelegate, SheetEditViewDelegate, GRRequestsManagerDelegate>
{
    BOOL bChangedFlag;
    BOOL bFilteredFlag;
    MBProgressHUD *hud;

    UIResponder *currentResponder;
}

@property (nonatomic, retain) Sheet *worksheet;
@property (nonatomic, retain) NSMutableArray *sheetitemlist;
@property (nonatomic, copy) NSMutableArray *descList;
@property (nonatomic, retain) NSIndexPath *currRowInd;
@property (nonatomic, retain) NSNumber *alertMode;


@property (weak, nonatomic) IBOutlet UILabel *lblSheetTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemCount;
@property (weak, nonatomic) IBOutlet UITextField *txtItemId;

@property (weak, nonatomic) IBOutlet UITableView *tblviewObject;
@property (nonatomic, retain) NSMutableArray *tblData;
@property (nonatomic, retain) NSMutableArray *tblFilterData;

@property (nonatomic, retain) id<SheetListViewDelegate> delegate;

- (void)initializeList;
- (void)reloadTableDatas;

- (IBAction)OnSearch:(id)sender;
- (IBAction)OnNew:(id)sender;
- (IBAction)OnExport:(id)sender;
- (IBAction)OnUpload:(id)sender;
- (IBAction)OnHelp:(id)sender;
- (IBAction)OnClose:(id)sender;
- (NSString *)GetNewItemID:(NSString *) sheetname;
-(SheetItem *)getLastSheetItemFromID:(NSString *)sheetName currentId:(NSString *) itemId;
@end
