//
//  ViewController.h
//  List Assist
//
//  Created by Admin on 9/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LicenseViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *btnHere;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreeCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnProceed;

- (IBAction)OnHereClick:(id)sender;
- (IBAction)OnAgreeClick:(id)sender;
- (IBAction)OnProceedClick:(id)sender;

- (void)showWorkSheetListView;

@end
