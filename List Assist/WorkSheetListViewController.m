//
//  WorkSheetListViewController.m
//  List Assist
//
//  Created by Admin on 9/15/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "WorkSheetListViewController.h"
#import "Common.h"
#import "Global.h"
#import "DataMgr.h"
#import "STDataInfo.h"
#import "WorkSheetAddViewController.h"
#import "SheetListViewController.h"
#import "AppDelegate.h"
#import "Sheet.h"
#import "HelpViewController.h"

@interface WorkSheetListViewController ()<NSFetchedResultsControllerDelegate>
{
    NSInteger deleteIndex;
    
}
@end

@implementation WorkSheetListViewController

@synthesize sheetlist;
@synthesize tblviewObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeList];

    // Do any additional setup after loading the view.
//    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
//    lpgr.minimumPressDuration = 1.0;
//    [self.tblviewObject addGestureRecognizer:lpgr];
}

- (void)handleLongPress:(UILongPressGestureRecognizer*) gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tblviewObject];
    
    NSIndexPath *indexPath = [tblviewObject indexPathForRowAtPoint:p];
    if(indexPath == nil)
    {
        NSLog(@"long press on table view but not on a row");
    }
    else
    {
        deleteIndex = indexPath.row;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                        message:@"Are you sure to delete this sheet?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            //here you pop the viewController
            [self deleteSheet];
            break;
    }
}

- (void)deleteSheet
{
    Sheet * iteminfo = [sheetlist objectAtIndex:deleteIndex];
    NSError *error=nil;
    [[self appDelegate].sheetDataContext deleteObject:iteminfo];
    if (![[self appDelegate ].sheetDataContext save:&error]) {
        //handle your error
    }
    
    [sheetlist removeObjectAtIndex:deleteIndex];
    [tblviewObject reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Called when the view is about to made visible. Default does nothing
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadSheetList];
}

// Called when the view has been fully transitioned onto the screen. Default does nothing
- (void)viewDidAppear:(BOOL)animated
{
}

// Called when the view is dismissed, covered or otherwise hidden. Default does nothing
- (void)viewWillDisappear:(BOOL)animated
{
    
}

// Called after the view was dismissed, covered or otherwise
- (void)viewDidDisappear:(BOOL)animated
{
    
}

- (void)initializeList
{
    tblviewObject.allowsMultipleSelectionDuringEditing = NO;
    
    [self loadSheetList];
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(NSUInteger)GetSheetItemCount:(NSString *)sheetname
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sheetname == %@", sheetname];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    
    return [fetchedObjects count];
}


-(void)loadSheetList
{
    if (sheetlist)
        sheetlist=nil;
    sheetlist=[[NSMutableArray alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Sheet"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    
    for(int i = 0;i<[fetchedObjects count];i++)
    {
        Sheet *obj = (Sheet *)[fetchedObjects objectAtIndex:i];
        NSUInteger icnt = [self GetSheetItemCount:obj.sheetname];
        obj.itemcount = [NSNumber numberWithInteger:icnt];
        
        [sheetlist addObject:obj];
    }
    [self.tblviewObject reloadData];
}

#pragma UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sheetlist count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"WorkSheetCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Sheet *worksheet = [sheetlist objectAtIndex:indexPath.row];
    
    UILabel *lblCount = (UILabel *)[cell viewWithTag:100];
    
    //Comment by Zhongyu
    lblCount.text = [NSString stringWithFormat:@"%d", [worksheet.itemcount intValue] ];
    
    UILabel *lblSheetName = (UILabel *)[cell viewWithTag:101];
    lblSheetName.text = worksheet.sheetname;
    
    UILabel *lblDescription= (UILabel *)[cell viewWithTag:102];
    lblDescription.text = worksheet.sheetdesc;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sheet *worksheet = (Sheet *)[sheetlist objectAtIndex:indexPath.row];
    
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    
    SheetListViewController *sheetlistVC = (SheetListViewController *)[board instantiateViewControllerWithIdentifier:@"SheetList"];
    
    sheetlistVC.worksheet = worksheet;
    sheetlistVC.delegate = self;
    
    [self.navigationController pushViewController:sheetlistVC animated
                                                 :YES];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        deleteIndex = indexPath.row;
        //add code here for when you hit delete
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                        message:@"If you delete this sheet all of items belong the sheet will be deleted automatically.\nAre you still want to delete this sheet?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
    }
}

- (IBAction)OnAddWorkSheet:(id)sender
{
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    
    WorkSheetAddViewController *wsAddVC = (WorkSheetAddViewController *)[board instantiateViewControllerWithIdentifier:@"WorkSheetAdd"];
    
    wsAddVC.delegate = self;
    
    [self.navigationController pushViewController:wsAddVC animated:YES];
}

- (IBAction)OnImportWorkSheet:(id)sender
{
    
}

- (IBAction)OnHelp:(id)sender {
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    HelpViewController *wsAddVC = (HelpViewController *)[board instantiateViewControllerWithIdentifier:@"HelpList"];
    [self.navigationController pushViewController:wsAddVC animated:YES];
}


#pragma WorkSheetListViewController Delegate

- (void)OnSave:(WorkSheetAddViewController *)worksheetaddVC
{
    [self.navigationController popViewControllerAnimated:YES];

    [self initializeList];
}

- (void)OnCancel:(WorkSheetAddViewController *)worksheetaddVC
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma SheetListViewController Delegate
- (void)OnChangedClose:(SheetListViewController *)sheetlistVC
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self initializeList];
}

- (void)OnClose:(SheetListViewController *)sheetlistVC
{
    [self.navigationController popViewControllerAnimated:YES];    
}
@end
