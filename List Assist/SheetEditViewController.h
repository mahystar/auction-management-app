//
//  SheetEditViewController.h
//  List Assist
//
//  Created by Admin on 9/19/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AssetsLibrary/AssetsLibrary.h>

#import "STDataInfo.h"
#import "ImageCropView.h"
#import "PhotoEditViewController.h"
#import "SheetItem.h"
#import "AppDelegate.h"
#import "TPKeyboardAvoidingScrollView.h"
@class SheetEditViewController;
@class SheetListViewController;

@protocol SheetEditViewDelegate

- (void)OnSheetEditClose:(SheetEditViewController *)sheeteditVC;
- (void)OnSheetEditSave:(SheetEditViewController *)sheeteditVC;

@end

@interface SheetEditViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIImagePickerControllerDelegate, ImageCropViewControllerDelegate, PhotoEditViewDelegate>
{
    UIResponder *currentResponder;
    
    ALAssetsLibrary *_assetLibrary;
    
    dispatch_queue_t _serialQueue;
//    UITableView *autocompleteTableView;
}

@property (nonatomic, retain) NSString *sheetTitle;
@property (nonatomic, retain) NSString *sheetname;
@property (nonatomic, retain) SheetItem *sheetitem;
@property (nonatomic, retain) NSMutableArray *sheetitemlist;
@property (nonatomic, retain) NSMutableArray *photos;
@property (nonatomic, retain) NSMutableArray *thumbnails;
@property (nonatomic, retain) NSMutableArray *descriptionList;
@property (nonatomic, retain) NSMutableArray *autocompleteDescs;
@property (nonatomic, retain) NSNumber *currIndex;
@property (nonatomic, retain) SheetListViewController *listViewController;
@property (nonatomic, retain) UITableView *autocompleteTableView;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollViewPanel;

@property (weak, nonatomic) IBOutlet UILabel *lblSheetEditTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtItemNum;

@property (weak, nonatomic) IBOutlet UITextField *txtItem;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UITextView *txtAddDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtTaxable;
@property (weak, nonatomic) IBOutlet UITextField *txtMSB;
@property (weak, nonatomic) IBOutlet UITextField *txtReservePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
@property (weak, nonatomic) IBOutlet UITextField *txtSeller;
@property (weak, nonatomic) IBOutlet UITextField *txtBuyNowPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtPhotos;
@property (weak, nonatomic) IBOutlet UITextField *txtThumbnail;
@property (weak, nonatomic) IBOutlet UITextField *txtCopyItem;

@property (nonatomic, retain) id<SheetEditViewDelegate> delegate;

- (void)initializeList;
//- (void)initDescriptionList;

- (IBAction)OnPhoto:(id)sender;
- (IBAction)OnPhotoEdit:(id)sender;
- (IBAction)OnDelete:(id)sender;

- (IBAction)OnCopyItem:(id)sender;
- (IBAction)OnPrev:(id)sender;
- (IBAction)OnNew:(id)sender;
- (IBAction)OnSave:(id)sender;
- (IBAction)OnClose:(id)sender;
- (IBAction)OnLast:(id)sender;

@end
