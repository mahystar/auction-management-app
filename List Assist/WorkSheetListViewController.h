//
//  WorkSheetListViewController.h
//  List Assist
//
//  Created by Admin on 9/15/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WorkSheetAddViewController.h"
#import "SheetListViewController.h"

@interface WorkSheetListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, WorkSheetAddViewDelegate, SheetListViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblviewObject;
@property (nonatomic, retain) NSMutableArray *sheetlist;


- (IBAction)OnAddWorkSheet:(id)sender;
- (IBAction)OnImportWorkSheet:(id)sender;
- (IBAction)OnHelp:(id)sender;


@end
