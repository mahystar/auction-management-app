//
//  PhotoEditViewController.h
//  List Assist
//
//  Created by Admin on 10/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PhotoEditViewController;

@protocol PhotoEditViewDelegate

- (void)OnPhotoEditClose:(PhotoEditViewController *)photoeditVC;

@end

@interface PhotoEditViewController : UIViewController
{
    NSString *strPhotoPath;
}

@property (nonatomic, retain) NSMutableArray *photos;
@property (nonatomic, retain) NSMutableArray *thumbnails;
@property (nonatomic, retain) NSNumber *currIndex;
@property (nonatomic, retain) NSString *sheetname;

@property (nonatomic, retain) IBOutlet UIImageView *imgMain;
@property (nonatomic, retain) IBOutlet UIImageView *imgThumb;
@property (nonatomic, retain) IBOutlet UIButton *btnPrev;
@property (nonatomic, retain) IBOutlet UILabel *lblPageNum;
@property (nonatomic, retain) IBOutlet UIButton *btnNext;

@property (nonatomic, retain) id<PhotoEditViewDelegate> delegate;

//- (void)initializeList;

- (IBAction)OnPrev:(id)sender;
- (IBAction)OnNext:(id)sender;
- (IBAction)OnRemove:(id)sender;
- (IBAction)OnClose:(id)sender;

@end
