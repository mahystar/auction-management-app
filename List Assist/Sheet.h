//
//  Sheet.h
//  List Assist
//
//  Created by Admin on 11/22/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Sheet : NSManagedObject

@property (nonatomic, retain) NSString * sheetdesc;
@property (nonatomic, retain) NSString * sheetname;
@property (nonatomic, retain) NSNumber * itemcount;

@end
