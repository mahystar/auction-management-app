//
//  PhotoEditViewController.m
//  List Assist
//
//  Created by Admin on 10/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "PhotoEditViewController.h"
#import "AppDelegate.h"
#import "Common.h"

@interface PhotoEditViewController ()

@end

@implementation PhotoEditViewController

@synthesize imgMain, imgThumb, btnPrev, lblPageNum, btnNext;
@synthesize thumbnails, photos, currIndex, sheetname;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    currIndex = [NSNumber numberWithInt:1];
    [self showImage:[NSNumber numberWithInt:1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)getPagiNavString
{
    NSString *rst = nil;
    if ([photos count] > 0) {
        rst = [NSString stringWithFormat:@"%d/%d", [currIndex intValue], [photos count]];
    } else {
        rst = [NSString stringWithFormat:@"%d/%d", 0, 0];
    }
    
    return rst;
}

- (void)showImage:(NSNumber *)index
{
    [lblPageNum setText:[self getPagiNavString]];
    
    if ([photos count] > 0) {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        docDir = [docDir stringByAppendingPathComponent:sheetname];
        
        // Get the Image File Path
        NSString *photoFilePath = [docDir stringByAppendingPathComponent: [photos objectAtIndex:[index intValue] - 1] ];
        UIImage *img = [UIImage imageWithContentsOfFile:photoFilePath];
        
        [imgMain setImage:img];
        
        // Get the Image File Path
        NSString *thumbFilePath = [docDir stringByAppendingPathComponent: [thumbnails objectAtIndex:[index intValue] - 1] ];
        UIImage *imgthumb = [UIImage imageWithContentsOfFile:thumbFilePath];
        [imgThumb setImage:imgthumb];
        
        CGFloat scaleRate = 0.0, scaleRateT = 0.0, sRate = 0.0;
        scaleRate = 205 / 175;
        scaleRateT = img.size.width / img.size.height;
        
        CGRect rect = CGRectMake(0, 0, 1, 1);
        if (scaleRateT <= scaleRate) {
            sRate = 175 / img.size.height;
        } else {
            sRate = 205 / img.size.width;
        }
        rect = CGRectMake(imgMain.frame.origin.x, imgMain.frame.origin.y, img.size.width * sRate, img.size.height * sRate);
        
        imgMain.frame = rect;
        
        
        //    scaleRate = 205 / 175;
        scaleRate = 150 / 114;
        scaleRateT = imgthumb.size.width / imgthumb.size.height;
        
        if (scaleRateT <= scaleRate) {
            sRate = 114 / imgthumb.size.height;
        } else {
            sRate = 150 / imgthumb.size.width;
        }
        rect = CGRectMake(imgThumb.frame.origin.x, imgThumb.frame.origin.y, imgthumb.size.width * sRate, imgthumb.size.height * sRate);
        
        imgThumb.frame = rect;
    } else {
        [imgMain setImage:nil];
        [imgThumb setImage:nil];
    }
}

- (IBAction)OnPrev:(id)sender {
    if ([currIndex intValue] > 1) {
        currIndex = [NSNumber numberWithInt:[currIndex intValue] - 1];
        [self showImage:currIndex];
    }
}

- (IBAction)OnNext:(id)sender {
    if ([currIndex intValue] < [photos count]) {
        currIndex = [NSNumber numberWithInt:[currIndex intValue] + 1];
        [self showImage:currIndex];
    }
}

- (IBAction)OnRemove:(id)sender {
    [self removeImage:[photos objectAtIndex:[currIndex intValue] -1]];
    [self removeImage:[thumbnails objectAtIndex:[currIndex intValue] -1]];
    
    [photos removeObjectAtIndex:[currIndex intValue] - 1];
    [thumbnails removeObjectAtIndex:[currIndex intValue] - 1];
    
    if ([currIndex intValue] > 1) {
        currIndex = [NSNumber numberWithInt:[currIndex intValue] - 1];
    }
    
    UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [removeSuccessFulAlert show];

    [self showImage:currIndex];
}

- (BOOL)removeImage:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        return true;
    }
    else
    {
        return false;
    }
}

- (IBAction)OnClose:(id)sender {
    
    if (self.delegate) {
        [self.delegate OnPhotoEditClose:self];
    }
    else
        [self.navigationController popViewControllerAnimated:NO];
}

@end
