//
//  SheetListViewController.m
//  List Assist
//
//  Created by Admin on 9/19/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "SheetListViewController.h"

#import "Common.h"
#import "Global.h"
#import "STDataInfo.h"
#import "SheetEditViewController.h"
#import "SheetItem.h"
#import "LastSheetItem.h"
#import "CHCSVParser.h"

@interface SheetListViewController () <UIGestureRecognizerDelegate>
{
    int _uploadStage;
    int _uploadCnt;
    int _uploadMode;
    NSString *csvpath;
    NSString *targetDir;
    NSString *targetPath;
}

@property (nonatomic, strong) GRRequestsManager *requestsManager;

@end

@implementation SheetListViewController

@synthesize currRowInd;
@synthesize worksheet;
@synthesize sheetitemlist;
@synthesize descList;
@synthesize lblSheetTitle;
@synthesize lblItemCount;
@synthesize txtItemId;
@synthesize tblData;
@synthesize tblFilterData;
@synthesize tblviewObject;
@synthesize alertMode;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    bChangedFlag = false;
    bFilteredFlag = false;
    currRowInd = [[NSIndexPath alloc] initWithIndex:0];
    _uploadMode = 1;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];    
}

// Called when the view is about to made visible. Default does nothing
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initializeList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)initializeList
{
    [lblSheetTitle setText:worksheet.sheetname];

    if (sheetitemlist)
        sheetitemlist=nil;
    sheetitemlist=[[NSMutableArray alloc] init];
    
    if (descList)
        descList=nil;
    descList=[[NSMutableArray alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sheetname == %@", worksheet.sheetname];
    [fetchRequest setPredicate:predicate];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item" ascending:NO]];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    for(int i = 0;i<[fetchedObjects count];i++)
    {
        SheetItem *obj = (SheetItem *)[fetchedObjects objectAtIndex:i];
        [sheetitemlist addObject:obj];
        
    }
    
    
    tblData = [[NSMutableArray alloc] init];
    tblData = [sheetitemlist mutableCopy];
    
    for (int j = 0; j < [tblData count]; j++) {
        SheetItem *sitem = (SheetItem *)[tblData objectAtIndex:j];
        NSString *desc = [sitem description];
//        NSString *desc = [NSString stringWithFormat:@"%@", sitem.description];
        [descList addObject:desc];
    }
    
    [self reloadTableDatas];
    
    [lblItemCount setText:[NSString stringWithFormat:@"%lu", (unsigned long)[tblData count]]];
}

- (void)reloadTableDatas
{
    [tblviewObject reloadData];
}

- (void)getAllSheetItems:(NSString *)sheetName
{
    
}

#pragma UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (bFilteredFlag)
        return [tblFilterData count];
    else
        return [tblData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"WorkSheetCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell setBackgroundColor:[UIColor lightGrayColor]];
    
    SheetItem *sheetitem;
    if (bFilteredFlag)
        sheetitem = (SheetItem *)[tblFilterData objectAtIndex:indexPath.row];
    else
        sheetitem = (SheetItem *)[tblData objectAtIndex:indexPath.row];

    cell.textLabel.text = [NSString stringWithFormat:@"%@-%@-%@", sheetitem.item, sheetitem.category, sheetitem.desc];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:@"Selected Value is %@",[tblData objectAtIndex:indexPath.row]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [alertView show];*/
    
    // Get the selected sheet item.
    // Add by Zhongyu
    
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    SheetEditViewController *sheeteditVC = (SheetEditViewController *)[board instantiateViewControllerWithIdentifier:@"SheetEdit"];
    
    SheetItem * iteminfo = [tblData objectAtIndex:indexPath.row];
    
    sheeteditVC.sheetTitle = @"Edit Item";
    sheeteditVC.sheetitem = iteminfo;
    sheeteditVC.sheetname = lblSheetTitle.text;
    sheeteditVC.sheetitemlist = sheetitemlist;
    sheeteditVC.currIndex = [[NSNumber alloc] initWithInteger:indexPath.row];
    sheeteditVC.delegate = self;
    sheeteditVC.listViewController = self;
    sheeteditVC.descriptionList = descList;
    
    [self.navigationController pushViewController:sheeteditVC animated:YES];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //Set delete Confirm flag
        alertMode = [NSNumber numberWithInt:1];
        
        currRowInd = indexPath;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"List Assist"
                                                        message:@"Are you sure to delete this item?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];

        //add code here for when you hit delete
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertMode intValue] == 2) {    //Export CSV Mode
        NSString *pwd = [[alertView textFieldAtIndex:0] text];
        
//        if ([pwd isEqualToString:@"BiD!585AsSeTs!"]) {
        if ([pwd isEqualToString:@"q"]) {
            [self SaveCSVFile];
        } else {
            alertMode = [NSNumber numberWithInt:3];
            UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Fail:" message:@"The password is incorrect." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [removeSuccessFulAlert show];
        }
    } else if ([alertMode intValue] == 1)   //Delete Confirm Mode
    {
        switch(buttonIndex) {
            case 0: //"No" pressed
                //do something?
                break;
            case 1: //"Yes" pressed
                //here you pop the viewController
                //[self.navigationController popViewControllerAnimated:YES];
                [self deleteSheetItem];
                break;
        }
    }
    else if ([alertMode intValue] == 4) {
        switch(buttonIndex) {
            case 0: //"No" pressed
                _uploadMode = 0;
                //do something?
                break;
            case 1: //"Yes" pressed
                _uploadMode = 1;
                break;
        }
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";

        NSString *dirname = [lblSheetTitle.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSString *fname = [NSString stringWithString:dirname];
        NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"item" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
        
        [sheetitemlist sortUsingDescriptors:sortDescriptors];
        
        if ([sheetitemlist count] > 0) {
            SheetItem *sheetItem = [sheetitemlist objectAtIndex:0];
            
            fname = [NSString stringWithFormat:@"%@_%@", dirname, [self getItemPaddNum:sheetItem.item]];
        }
        
        targetDir = [[NSString stringWithFormat:@"/html/App_Inventory_Uploads/%@/", dirname] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        targetPath = [[NSString stringWithFormat:@"%@%@.csv", targetDir, fname] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        
        //    targetPath = [[targetDir stringByAppendingString:lblSheetTitle.text] stringByAppendingPathExtension:@"csv"];
        
        [self setupFTPManager];
//        [self requestListDirectory];
        [self requestDeleteCacheCSV];
        [self requestCreateTargetDir];
        [self requestUploadCSV];
        
    }
    
}

- (void)deleteSheetItem
{
    SheetItem * iteminfo = [tblData objectAtIndex:currRowInd.row];
    NSError *error=nil;
    [[self appDelegate].sheetDataContext deleteObject:iteminfo];
    if (![[self appDelegate ].sheetDataContext save:&error]) {
        //handle your error
    }

    [tblData removeObjectAtIndex:currRowInd.row];
    [self reloadTableDatas];
}

# pragma mark Gesture selector
- (void)backgroundTap:(UITapGestureRecognizer *)backgroundTap {
    if(currentResponder){
        [currentResponder resignFirstResponder];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    currentResponder = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    currentResponder = nil;
}

#pragma mark Keyboard Methods

- (void)keyboardShowing:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardHiding:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}


- (IBAction)OnSearch:(id)sender
{
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }

    if(txtItemId.text.length > 0)
    {
        bFilteredFlag = true;
        
        tblFilterData = [[NSMutableArray alloc] init];
        
        for (SheetItem *item in tblData)
        {
            if ([txtItemId.text isEqualToString:item.item])
            {
                [tblFilterData addObject:item];
            }
        }
    }
    else
    {
        bFilteredFlag = false;
    }

    [tblviewObject reloadData];    
}

- (NSString *)GetNewItemID:(NSString *) sheetname
{
    NSNumber *rst = [[NSNumber alloc] initWithInt:1];
    BOOL needMaxVal = false;
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LastSheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sheetname == %@", sheetname];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    
    //If there exists some items which last saved, get last item.
    if ([fetchedObjects count] > 0) {
        LastSheetItem *lastitem = (LastSheetItem *)[fetchedObjects objectAtIndex:0];
        rst = [f numberFromString:lastitem.item];
        rst = [NSNumber numberWithInt:[rst intValue] + 1];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                                  inManagedObjectContext:[self appDelegate].sheetDataContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"sheetname == %@ AND item == %@", sheetname, rst];
        [fetchRequest setPredicate:pred1];
        [fetchRequest setEntity:entity];
        NSError *error = nil;
        NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
        if ([fetchedObjects count] > 0) {
            needMaxVal = true;
        }
    } else {
        needMaxVal = true;
    }
    
    if (needMaxVal) {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                                  inManagedObjectContext:[self appDelegate].sheetDataContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        fetchRequest.fetchLimit = 1;
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item" ascending:NO]];
        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"sheetname == %@", sheetname];
        [fetchRequest setPredicate:pred1];
        [fetchRequest setEntity:entity];
        NSError *error = nil;
        NSArray * itemarray = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
        
        if ([itemarray count] > 0) {
            SheetItem *iteminfo = [itemarray objectAtIndex:0];
            rst = [f numberFromString:iteminfo.item];
            rst = [NSNumber numberWithInt:[rst intValue] + 1];
        }
    }
    return [rst stringValue];
}

-(SheetItem *)getLastSheetItemFromID:(NSString *)sheetName currentId:(NSString *) itemId
{
    NSNumber *beforeid = [[NSNumber alloc] initWithInt:1];
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    beforeid = [f numberFromString:itemId];
    
    if ([beforeid intValue] > 1) {
        beforeid = [NSNumber numberWithInt:[beforeid intValue] - 1];
    }

    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"sheetname == %@ AND item == %@", sheetName, beforeid];
    [fetchRequest setPredicate:pred2];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] > 0) {
        return [fetchedObjects objectAtIndex:0];
    }
    
    return nil;
}

- (IBAction)OnNew:(id)sender {
    
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
    /* --- Add by Zhongyu ---
       Get the last sheet item and set is as newe item id.
     */
    SheetItem*  newitem =[NSEntityDescription
                       insertNewObjectForEntityForName:@"SheetItem"
                       inManagedObjectContext:[self appDelegate].sheetDataContext];
    
    NSString *newid = [self GetNewItemID:lblSheetTitle.text];
    newitem.item = newid;
    
    SheetItem *iteminfo = [self getLastSheetItemFromID:lblSheetTitle.text currentId:newid];
    if (iteminfo != nil) {
        newitem.category = iteminfo.category;
        newitem.taxable = iteminfo.taxable;
        newitem.minstartingbid = iteminfo.minstartingbid;
        newitem.quantity = iteminfo.quantity;
        newitem.seller = iteminfo.seller;
        newitem.buynowprice = iteminfo.buynowprice;
    }
    
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    
    SheetEditViewController *sheeteditVC = (SheetEditViewController *)[board instantiateViewControllerWithIdentifier:@"SheetEdit"];
    
    sheeteditVC.sheetTitle = @"Add Item";
    sheeteditVC.sheetitem = newitem;
    sheeteditVC.sheetname = lblSheetTitle.text;
    sheeteditVC.delegate = self;
    sheeteditVC.listViewController = self;
    
    [self.navigationController pushViewController:sheeteditVC animated:YES];
}

- (BOOL)removeCSV:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        return true;
    }
    else
    {
        return false;
    }
}

- (IBAction)OnExport:(id)sender {

    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
    //Set export alert flag
    alertMode = [NSNumber numberWithInt:2];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"List Assist" message:@"Please enter password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(NSString *)getItemPaddNum:(NSString *)item
{
    NSString *rst = [NSString stringWithString:item];
    
    if ([rst length] < 6) {
        while ([rst length] < 6) {
            NSString *tmp = [NSString stringWithFormat:@"0"];
            rst = [[tmp stringByAppendingString:@"0"] stringByAppendingString:rst];
        }
    }
    
    return rst;
}

-(NSString *)getCSVFilePath
{
    NSString *fpath = nil;
    NSString *fname = [NSString stringWithString:lblSheetTitle.text];
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    docDir = [docDir stringByAppendingPathComponent:lblSheetTitle.text];
    
    NSError *error = [[NSError alloc] init];
    if (![[NSFileManager defaultManager] fileExistsAtPath:docDir])
        [[NSFileManager defaultManager] createDirectoryAtPath:docDir withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder

    NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"item" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
    
    [sheetitemlist sortUsingDescriptors:sortDescriptors];
    
    if ([sheetitemlist count] > 0) {
        SheetItem *sheetItem = [sheetitemlist objectAtIndex:0];
        
        fname = [NSString stringWithFormat:@"%@_%@", lblSheetTitle.text, [self getItemPaddNum:sheetItem.item]];
    }
    
    fpath = [docDir stringByAppendingPathComponent: [fname stringByAppendingPathExtension:@"csv"]];

    return fpath;
}

-(void) SaveCSVFile
{
    NSString *fpath = [self getCSVFilePath];
    
    BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
    
    if (aExist) {
        [self removeCSV:fpath];
    }
    
    CHCSVWriter *writer = [[CHCSVWriter alloc] initForWritingToCSVFile:fpath];
    
    NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"item" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
    
    [sheetitemlist sortUsingDescriptors:sortDescriptors];

    [writer writeHeader];
    
    for (SheetItem *iteminfo in sheetitemlist) {
        
        [writer writeLineOfFields:iteminfo ];
    }
    
    [writer closeStream];
    
    alertMode = [NSNumber numberWithInt:3];
    UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"The sheet items are successfully exported." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [removeSuccessFulAlert show];
}

- (IBAction)OnUpload:(id)sender {

    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
    [self uploadSheetFile];
}

- (void)setupFTPManager
{
    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"bid-assets.com"
                                                                  user:@"bidassets"
                                                              password:@"red1208"];

//    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"218.25.89.125"
//                                                                  user:@"weiyingdev"
//                                                              password:@"gidshddprh"];

    self.requestsManager.delegate = self;
}

-(void) uploadSheetFile
{
    csvpath = [self getCSVFilePath];
    
    BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:csvpath];
    
    if (!aExist) {
        [Common showToastWithMessage:@"Please export before upload." parent:self];
        return;
    }
    
    alertMode = [NSNumber numberWithInt:4]; //Upload mode
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"List Assist"
                                                    message:@"Are you going to upload photos together?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

-(void) requestListDirectory
{
    _uploadStage = 0;
    [self.requestsManager addRequestForListDirectoryAtPath: targetDir ];
    [self.requestsManager startProcessingRequests];
}

-(void) requestCreateTargetDir
{
    _uploadStage = 1;
    [self.requestsManager addRequestForCreateDirectoryAtPath: targetDir ];
    [self.requestsManager startProcessingRequests];
}

- (void) requestDeleteCacheCSV
{
    NSString *fname = [NSString stringWithString:lblSheetTitle.text];
    NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"item" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
    
    [sheetitemlist sortUsingDescriptors:sortDescriptors];

    for (SheetItem *item in sheetitemlist) {
        
        NSString *dirname = [lblSheetTitle.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        fname = [NSString stringWithFormat:@"%@_%@", dirname, [self getItemPaddNum:item.item]];
        
        NSString *ftpPath = [[NSString stringWithFormat:@"%@%@.csv", targetDir, fname] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        
        [self.requestsManager addRequestForDeleteFileAtPath: ftpPath];
    }

    [self.requestsManager startProcessingRequests];
}

- (void) requestDeleteCache: (NSArray *) listing
{
    _uploadStage = 2;

    for (NSString *filename in listing)
    {
        NSString *tmpname = [[NSString stringWithFormat:@"%@%@", targetDir, filename] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];

        [self.requestsManager addRequestForDeleteFileAtPath: tmpname];
//        [self.requestsManager startProcessingRequests];
    }

    [self.requestsManager addRequestForDeleteDirectoryAtPath:targetDir];
    [self.requestsManager startProcessingRequests];
}

-(void) requestUploadCSV
{
    _uploadStage = 3;
    _uploadCnt = 1;
    [self.requestsManager addRequestForUploadFileAtLocalPath:csvpath toRemotePath:targetPath];
    
    NSString *fpath = nil;
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    docDir = [docDir stringByAppendingPathComponent:lblSheetTitle.text];
    
    NSError *error = [[NSError alloc] init];
    if (![[NSFileManager defaultManager] fileExistsAtPath:docDir])
        [[NSFileManager defaultManager] createDirectoryAtPath:docDir withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    if (_uploadMode == 1) {
        for (SheetItem *item in sheetitemlist) {
            NSMutableArray *photos = [[NSMutableArray alloc] init];
            if (item.photos != nil && [item.photos length] > 0) {
                [photos addObjectsFromArray:[item.photos componentsSeparatedByString:@","]];
                for (NSString *imgname in photos)
                {
                    NSString *imgpath = [[NSString stringWithFormat:@"%@%@", targetDir, imgname] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
                    fpath = [docDir stringByAppendingPathComponent: imgname];
                    [self.requestsManager addRequestForUploadFileAtLocalPath:fpath toRemotePath:imgpath];
                    [self.requestsManager startProcessingRequests];
                    _uploadCnt++;
                }
            }

            NSMutableArray *thumbs = [[NSMutableArray alloc] init];
            if (item.thumbnails != nil && [item.thumbnails length] > 0) {
                [thumbs addObjectsFromArray:[item.thumbnails componentsSeparatedByString:@","]];
                for (NSString *imgname in thumbs)
                {
                    NSString *imgpath = [[NSString stringWithFormat:@"%@%@", targetDir, imgname] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
                    fpath = [docDir stringByAppendingPathComponent: imgname];
                    [self.requestsManager addRequestForUploadFileAtLocalPath:fpath toRemotePath:imgpath];
                    [self.requestsManager startProcessingRequests];
                    _uploadCnt++;
                }
            }
        }
    }
    
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didCompleteListingRequest:(id<GRRequestProtocol>)request listing:(NSArray *)listing
{
    if (_uploadStage == 0) {
        [self requestDeleteCache:listing];
    }
    NSLog(@"requestsManager:didCompleteListingRequest:listing: \n%@", listing);
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didCompleteCreateDirectoryRequest:(id<GRRequestProtocol>)request
{
    //NSLog(@"requestsManager:didCompleteCreateDirectoryRequest:");
    if (_uploadStage == 1) {
        [self requestUploadCSV];
    }
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didCompleteDeleteRequest:(id<GRRequestProtocol>)request
{
    if (_uploadStage == 2) {
        [self requestCreateTargetDir];
    }

    NSLog(@"requestsManager:didCompleteDeleteRequest:");
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didCompletePercent:(float)percent forRequest:(id<GRRequestProtocol>)request
{
    NSLog(@"requestsManager:didCompletePercent:forRequest: %f", percent);
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didCompleteUploadRequest:(id<GRDataExchangeRequestProtocol>)request
{
    _uploadCnt--;

    if (_uploadCnt == 0) {
        [MBProgressHUD hideHUDForView:self.view animated:YES ];
        [Common showToastWithMessage:@"Uploaded successfully!" parent:self];
    }

    //NSLog(@"requestsManager:didCompleteUploadRequest:");
}

- (void)requestsManager:(id<GRRequestsManagerProtocol>)requestsManager didFailRequest:(id<GRRequestProtocol>)request withError:(NSError *)error
{
    if (_uploadStage == 0) {    //List Directory Error
        [MBProgressHUD hideHUDForView:self.view animated:YES ];
        //[self requestCreateTargetDir];
    } else if (_uploadStage == 1) { //Create Directory Error
        //[Common showToastWithMessage:@"Cannot create directory in FTP server." parent:self];
    } else if (_uploadStage == 2) { //Delete dir Error
        [Common showToastWithMessage:@"Cannot delete directory exists to FTP server." parent:self];
    } else if (_uploadStage == 3) { //Upload CSV Error
        //[MBProgressHUD hideHUDForView:self.view animated:YES ];
        //[Common showToastWithMessage:@"Cannot upload csv file to FTP server." parent:self];
    }
    NSLog(@"requestsManager:didFailRequest:withError: \n %@", error);
}

- (IBAction)OnHelp:(id)sender {
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    HelpViewController *wsAddVC = (HelpViewController *)[board instantiateViewControllerWithIdentifier:@"HelpList"];
    [self.navigationController pushViewController:wsAddVC animated:YES];
}

- (IBAction)OnClose:(id)sender {
    
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }

    if (self.delegate)
    {
        if (bChangedFlag)
            [self.delegate OnChangedClose:self];
        else
            [self.delegate OnClose:self];
    }
    else
        [self.navigationController popViewControllerAnimated:NO];
}

#pragma SheetEditViewController Delegate
- (void)OnSheetEditClose:(SheetEditViewController *)sheeteditVC
{
    [sheeteditVC.navigationController popViewControllerAnimated:YES];
}

- (void)OnSheetEditSave:(SheetEditViewController *)sheeteditVC
{
    [sheeteditVC.navigationController popViewControllerAnimated:YES];
    
    // Get all sheetitems by worksheet.sheetName.
    [self getAllSheetItems:worksheet.sheetname];

    [self initializeList];
}

#pragma mark - gesture delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (currentResponder)
        return YES;
    
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return NO;
}

@end
