//
//  WorkSheetAddViewController.h
//  List Assist
//
//  Created by Admin on 9/15/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sheet.h"
#import "HelpViewController.h"

@class WorkSheetAddViewController;

@protocol WorkSheetAddViewDelegate

- (void)OnSave:(WorkSheetAddViewController *)worksheetaddVC;
- (void)OnCancel:(WorkSheetAddViewController *)worksheetaddVC;

@end


@interface WorkSheetAddViewController : UIViewController<UITextFieldDelegate>
{
    UIResponder *currentResponder;
}

@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtNote;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (nonatomic, retain) id<WorkSheetAddViewDelegate> delegate;

- (IBAction)OnSave:(id)sender;
- (IBAction)OnExit:(id)sender;
- (IBAction)OnHelp:(id)sender;

@end
