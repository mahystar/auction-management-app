//
//  Common.m
//  4S-C
//
//  Created by R CJ on 1/5/13.
//  Copyright (c) 2013 PIC. All rights reserved.
//

#import "Common.h"
//#import "Defines.h"
#import <sys/utsname.h>
//#import "Base64.h"

#import "TRSProgressView.h"

// Common variables

NSString *  _deviceToken = @"";

#define _MAXLENGTH       50

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@implementation Common

+ (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"x86_64"    :@"Simulator",
                              @"i386"      :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",      // (Original)
                              @"iPod2,1"   :@"iPod Touch",      // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",      // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",      // (Fourth Generation)
                              @"iPhone1,1" :@"iPhone",          // (Original)
                              @"iPhone1,2" :@"iPhone",          // (3G)
                              @"iPhone2,1" :@"iPhone",          // (3GS)
                              @"iPad1,1"   :@"iPad",            // (Original)
                              @"iPad2,1"   :@"iPad 2",          //
                              @"iPad3,1"   :@"iPad",            // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",        //
                              @"iPhone4,1" :@"iPhone 4S",       //
                              @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",            // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",       // (Original)
                              @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",       // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini"        // (2nd Generation iPad Mini - Cellular)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
    }
    
    return deviceName;
}

+ (UIView*) waitView {
    return nil;
}

//+ (NSString*)relationAtIndex:(NSInteger)index {
//    return [[Common relationArray] objectAtIndex:index];
//}

+ (NSInteger)intWithRelation:(NSString*)relation {
//    for(NSInteger i = 0; i < [Common relationArray].count; i ++) {
//        if([relation compare:[[Common relationArray] objectAtIndex:i]] == NSOrderedSame)
//            return i;
//    }
    
    return 0;
}

//+ (NSString*)stringWithRelation:(NSString*)relation {
//    return [NSString stringWithFormat:@"%d", [Common intWithRelation:relation]];
//}

//+ (NSString*)timeStringWithSecond:(NSInteger)second {
//    if (second < 60)
//        return [NSString stringWithFormat:@"  %d\"", second];
//    else if (second < 3600)
//        return [NSString stringWithFormat:@"  %d'%d\"", second / 60, second % 60];
//    else
//        return [NSString stringWithFormat:@"  %dh%d'%d\"", second / 3600, (second % 3600) / 60, second % 60];
//}

+ (NSDateComponents*)dateComponentsFromString:(NSString*)string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    //NSDate *date = [dateFormatter dateFromString:string];
    NSDate *date = [dateFormatter dateFromString:[string substringToIndex:10]];
    NSDateComponents *dateCom = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
    
    return dateCom;
}

//+ (NSString*)dateStringFromString:(NSString*)string {
//    NSDateComponents *dateCom = [Common dateComponentsFromString:string];
//    return [NSString stringWithFormat:@"%d-%d-%d", dateCom.year, dateCom.month, dateCom.day];
//}

+ (UIImage*)resizeImage:(UIImage*)image withWidth:(int)width withHeight:(int)height
{
    CGSize newSize = CGSizeMake(width, height);
    float widthRatio = newSize.width/image.size.width;
    float heightRatio = newSize.height/image.size.height;
    
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


+ (void) showProgress:(UIView *)target
{
//	[DejalActivityView activityViewForView:target withLabel:STR_PLEASE_WAIT indicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
}

+ (void) hideProgress
{
//	[DejalActivityView removeView];
}

+ (BOOL) isIOSVer7
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        // code here
        return true;
    }
    
    return false;
}

+(BOOL)isKeyBoardInDisplay  {
    
    BOOL isExists = NO;
    for (UIWindow *keyboardWindow in [[UIApplication sharedApplication] windows])   {
        if ([[keyboardWindow description] hasPrefix:@"<UITextEffectsWindow"] == YES) {
            isExists = YES;
        }
    }
    
    return isExists;
}

+ (void) makeErrorWindow : (NSString *)content TopOffset:(NSInteger)topOffset BottomOffset:(NSInteger)bottomOffset View:(UIView *)view
{
    CGRect rt = [view frame];
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0., topOffset, rt.size.width, rt.size.height - topOffset - bottomOffset)];
    [imgView setImage:[UIImage imageNamed:@"bkError.png"]];
    
    UILabel * lblContent = [[UILabel alloc] initWithFrame:CGRectMake(0., topOffset, rt.size.width, rt.size.height - topOffset - bottomOffset)];
    lblContent.backgroundColor = [UIColor clearColor];
    lblContent.textAlignment = UITextAlignmentCenter;
    lblContent.text = content;
    
    [view addSubview:imgView];
    [view addSubview:lblContent];
}


+ (void) setDeviceToken : (NSString*)newDeviceToken
{
    _deviceToken = newDeviceToken;
}

+ (NSString*) deviceToken
{
    return _deviceToken;
}

+ (NSInteger) MAXLENGTH
{
    return _MAXLENGTH;
}

+ (NSString*) getCurTime : (NSString*)fmt
{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if ( fmt == nil ) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [dateFormatter setDateFormat:fmt];
    }
    
    return [dateFormatter stringFromDate:currentDate];
}

+ (NSInteger)phoneType
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([UIScreen mainScreen].bounds.size.height == 568) {
            return IPHONE5;
        }
        else {
            return IPHONE4;
        }
    }
    else {
        return IPAD;
    }
}

+ (NSString *)getRealImagePath :(NSString *)path :(NSString *)rate :(NSString *)size
{
    if (path.length > 0) {
        NSArray *pathArray = [path componentsSeparatedByString:@"/"];
        NSMutableString *realPath = [NSMutableString string];
        
        for (int i = 0; i < pathArray.count-1; i++) {
            [realPath appendString:[pathArray objectAtIndex:i]];
            [realPath appendString:@"/"];
        }
        
        [realPath appendString:@"640960"];
        [realPath appendString:@"_"];
        [realPath appendString:rate];
        [realPath appendString:@"_"];
        [realPath appendString:size];
        [realPath appendString:@"_"];
        [realPath appendString:[pathArray objectAtIndex:pathArray.count-1]];
        
        NSLog(@"%@", realPath);
        return realPath;
    }
    else {
        return @"";
    }
}

+ (NSString *)getBackImagePath :(NSString *)path :(NSString *)rate :(NSString *)size
{
    if (path.length > 0) {
        NSArray *pathArray = [path componentsSeparatedByString:@"/"];
        NSMutableString *realPath = [NSMutableString string];
        
        for (int i = 0; i < pathArray.count-1; i++) {
            [realPath appendString:[pathArray objectAtIndex:i]];
            [realPath appendString:@"/"];
        }
        
        if ([Common phoneType] == IPHONE5) {
            [realPath appendString:@"6401136"];
        }
        else {
            [realPath appendString:@"640960"];
        }
        [realPath appendString:@"_"];
        [realPath appendString:rate];
        [realPath appendString:@"_"];
        [realPath appendString:size];
        [realPath appendString:@"_"];
        [realPath appendString:[pathArray objectAtIndex:pathArray.count-1]];
        
        NSLog(@"%@", realPath);
        return realPath;
    }
    else {
        return @"";
    }
}

+ (NSString*)base64forData:(NSData*)theData 
{
#if true
//    return [Base64 encode:theData];
    return @"";
#else
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F]  :'=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F]  :'=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
#endif
}

+ (NSData*)base64forString:(NSString*)theString
{
    NSMutableData *mutableData = nil;

    if (theString) {
		unsigned long ixtext = 0;
		unsigned long lentext = 0;
		unsigned char ch = 0;
		unsigned char inbuf[4], outbuf[3];
		short i = 0, ixinbuf = 0;
		BOOL flignore = NO;
		BOOL flendtext = NO;
		NSData *base64Data = nil;
		const unsigned char *base64Bytes = nil;
        
		// Convert the string to ASCII data.
		base64Data = [theString dataUsingEncoding:NSASCIIStringEncoding];
		base64Bytes = [base64Data bytes];
		mutableData = [NSMutableData dataWithCapacity:[base64Data length]];
		lentext = [base64Data length];
        
		while( YES ) {
			if( ixtext >= lentext ) break;
			ch = base64Bytes[ixtext++];
			flignore = NO;
            
			if( ( ch >= 'A' ) && ( ch <= 'Z' ) ) ch = ch - 'A';
			else if( ( ch >= 'a' ) && ( ch <= 'z' ) ) ch = ch - 'a' + 26;
			else if( ( ch >= '0' ) && ( ch <= '9' ) ) ch = ch - '0' + 52;
			else if( ch == '+' ) ch = 62;
			else if( ch == '=' ) flendtext = YES;
			else if( ch == '/' ) ch = 63;
			else flignore = YES;
            
			if( ! flignore ) {
				short ctcharsinbuf = 3;
				BOOL flbreak = NO;
                
				if( flendtext ) {
					if( ! ixinbuf ) break;
					if( ( ixinbuf == 1 ) || ( ixinbuf == 2 ) ) ctcharsinbuf = 1;
					else ctcharsinbuf = 2;
					ixinbuf = 3;
					flbreak = YES;
				}
                
				inbuf [ixinbuf++] = ch;
                
				if( ixinbuf == 4 ) {
					ixinbuf = 0;
					outbuf [0] = ( inbuf[0] << 2 ) | ( ( inbuf[1] & 0x30) >> 4 );
					outbuf [1] = ( ( inbuf[1] & 0x0F ) << 4 ) | ( ( inbuf[2] & 0x3C ) >> 2 );
					outbuf [2] = ( ( inbuf[2] & 0x03 ) << 6 ) | ( inbuf[3] & 0x3F );
                    
					for( i = 0; i < ctcharsinbuf; i++ )
						[mutableData appendBytes:&outbuf[i] length:1];
				}
                
				if( flbreak )  break;
			}
		}
	}
    
	return mutableData;
}


+ (NSString *)appNameAndVersionNumberDisplayString 
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    //NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    return majorVersion;
}

+ (NSString *)weatherUrl
{
    //return @"http://m.weather.com.cn/data/";
    return @"http://api.sijimishu.com/weather.ashx?city=";
}

+ (NSString *) configFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
	NSString *docDir = [paths objectAtIndex:0];
	NSString *filePath = [NSString stringWithFormat:@"%@/", docDir];
	return filePath;
}

+ (NSString *) configFilePathWithFileName:(NSString *)fileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
	NSString *docDir = [paths objectAtIndex:0];
	NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDir, fileName];
	return filePath;
}

+ (void)showNotImplemented {
    [[TRSProgressView sharedInstance] presentWithText:@"NOT IMPLEMENTED" forSeconds:2.0 inFrame:CGRectMake(40, 200, 240, 100) withType:kAlertTypeInform withDescription:@""];
}

+ (void)showMessage : (NSString *)message {
    [[TRSProgressView sharedInstance] presentWithText:message forSeconds:2.0 inFrame:CGRectMake(40, 200, 240, 100) withType:kAlertTypeInform withDescription:@""];
}
#define kSlidingOffset  3.0

+ (void)setSliderOrigin:(UIView*)slidingView width:(CGFloat)width {
    for(UIView *view in slidingView.subviews) {
        if([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*)view;
            [label sizeToFit];
            CGFloat x = slidingView.frame.origin.x + slidingView.frame.size.width/2 - label.frame.size.width/2;
            CGRect rect;
            if(x < kSlidingOffset) {
                rect = label.frame;
                rect.origin.x = slidingView.frame.size.width/2 - label.frame.size.width/2 - (x-kSlidingOffset);
                label.frame = rect;
            } else if (x + label.frame.size.width <= width - kSlidingOffset) {
                rect = label.frame;
                rect.origin.x = slidingView.frame.size.width/2 - label.frame.size.width/2;
                label.frame = rect;
            } else {
                rect = label.frame;
                rect.origin.x = slidingView.frame.size.width/2 - label.frame.size.width/2 - (x+label.frame.size.width-width+kSlidingOffset);
                label.frame = rect;
            }
        }
    }
}

@end
