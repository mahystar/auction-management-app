
#import "HelpViewController.h"

@implementation HelpViewController

@synthesize detaillist;

- (void)awakeFromNib
{
    expandedRowIndex = -1;
    data = [NSMutableArray new];
    detaillist = [NSMutableArray new];

    [data addObject:@"- Welcome"];
    [data addObject:@"- THINGS YOU'LL NEED BEFORE STARTING"];
    [data addObject:@"- STEP ONE: CREATING YOUR AUCTION"];
    [data addObject:@"- STEP 2: IMPORTANT INSTRUCTIONS ON ITEM NUMBERS"];
    [data addObject:@"- STEP 3: ENTERING CATAGORIES AND DESCRIPTIONS"];
    [data addObject:@"- STEP 4: TAXES, STARTING BIDS, AND RESERVES"];
    [data addObject:@"- STEP 5: QUANTITY, SELLER, BUY NOW QUANTITY:"];
    [data addObject:@"- STEP 6: TAKING PHOTOS"];
    [data addObject:@"- COPY/PASTE/CLEAR"];
    [data addObject:@"- DELETE AN ITEM"];
    [data addObject:@"- EXPORTING and UPLOADING"];
    [data addObject:@"- Multiple Auctions on Phone"];
    [data addObject:@"- RECAPPING"];

    [detaillist addObject:@"Congratulations on downloading the List Assist app for your Android or iOS device. We believe that you will find this to be an efficient and powerful tool to inventory, photograph, and upload assets to the BidAssets.com online auction portal. Please read these simple instructions before using the List Assist app. It won't take long and it will give you valuable insight on how to most efficiently use this tool with ease. After a few items, you'll begin to see how simple this app is to use. By the way, you can use an Android or Apple Device, or a combination of both. List Assist doesn't care. Ready to begin? Let's go!"];

    [detaillist addObject:@"• Obviously your Android or iOS Phone or Tablets\n• A USB power bank battery for extended use. Your battery will be used up quickly while taking lots of photos and entering data.\n\t o USB power banks are available ANYWHERE: Wal-Mart, Best Buy, Costco, Sam's Club, Ebay, even major drug stores! Get one that is 4000 mAh or higher.\n• Tags and Markers...before taking your photos, make sure that your item has the same \"ITEM NUMBER\" tag as the \"ITEM NUMBER\" on your phone.\n"];

    [detaillist addObject:@"Go ahead to the Apps area of your phone or tablet and click on the List Assist Icon. You will see the terms of use for this App. Read them and if you agree, then check the box and click Proceed.\n\nGreat, now you're in. You'll see 2 buttons on the bottom: \"ADD\" and \"IMPORT\". Let's jump right in and ADD your first auction. Click ADD!\n\nWe're now at the ADD WorkSheet Page (A very important page)! Give your Worksheet (Auction) a very UNIQUE TITLE and then make notes below if you wish... I'll call mine ABC COMPANY SURPLUS 090915. Click Save.\n\nBEFORE WE GO FURTHER...PLEASE READ!\nIf you will be working as a team on a SINGLE AUCTION and using multiple phones, this TITLE name must be EXACTLY the same on each device. So, get with your team and make sure the TITLE is consistent across all relevant devices! Ok...is everyone's title the same? Good, now have everyone click Save.\n\nNow, let's get some work done. Click on your title name. At the bottom you'll see a button called \"NEW\". Click it and let's enter our first item! "];


    [detaillist addObject:@"BEFORE WE GO FURTHER...PLEASE LISTEN!\n ITEM: SEE THE ITEM NUMBER? If you will be working as a team on a SINGLE AUCTION and using multiple phones, ALL ITEM NUMBERS MUST BE DIFFERENT. So... TEAM MEMBER #1: why don't you allocate item numbers 1-100 and TEAM MEMBER #2: you start with item number 101-200 TEAM MEMBER #3: You'll start with item number 201 and end with 300. I really don't care how you divide up your numbers... AS LONG AS AN ITEM NUMBER IS NEVER, EVER, EVER, DUPLICATED! OK.... I think we're pretty clear on this now. Also, it is ok if numbers aren't used. If, for instance, TEAM MEMBER #1 only uses item numbers 1-60, and TEAM MEMBER #2 starts at 101, then the missing item numbers from 61-100 will not show at all on the auction. This is fine. The item number is simply a UNIQUE identifier for the item that you are selling. DID I MENTION NEVER DUPLICATE AN ITEM NUMBER? I thought so. If you will be doing the inventory by yourself, none of this matters, so let's move on!"];

    //step 3
    [detaillist addObject:@"BEFORE WE GO FURTHER...PLEASE LISTEN!\n There are several ways of entering the text data, including but not limited to \"Speech to Text\", \"Swyping\", or \"Typing.\" Use whatever is comfortable for you. I tend to like \"Speech to Text\", but beware: Many times what you say is not exactly what shows up, so make sure after you\'re done speaking to go in and make your corrections by typing! For example: instead of saying 25\" by 30\", SAY 25 X 30 (say the letter X) for dimensions. Let's move on.\n\nItem 1 is the default. At this point, Team Members should enter their beginning number as we just discussed. Do this now...I'll wait. Okay....ready? \n\nCATEGORY:\nClick \"NEXT\" on your phone or just touch the Category Line and enter (or Speak) a Category Type for your item such as: KITCHEN, FIXTURES, TOOLS, AUTOMOTIVE, HAZARDOUS WASTE.... it's entirely up to you. It may be a good idea now to stop and talk to your team about what category names you'll be using for certain items....you'll want to be on the same page here! You don't want an extreme amount of categories....this will just help the online buyer find items quicker based on Category....Now, Click Next or Touchthe Description Line.\n\nDESCRIPTION:\nYou have up to 500 characters to enter your description. It is VERY IMPORTANT to be consistent in your descriptions. I like to begin with Manufacturer, then Model Number, then Detailed Description, Size (H X W X D), and condition. Again, get with your Team Members and get a consistent game plan. It will make your auction much cleaner, easier for the online buyer to navigate, and ultimately MAKE MORE MONEY! The description is the most important, second only to the PHOTOS! Don't get lazy on me here.... let's give a great description for each and every item....PLEASE...it is worth it! NEXT!\n\nADDITIONAL DESCRIPTION:\nThis field can be used in a number of different ways (or not at all). Use for Item Location, Special Terms, Condition, Contact so and so for details at a certain phone #, Again.... it's up to you. It will not show in the main listing online. Only when the item is clicked will the Additional Description appear on the bottom of the item page. You have 100 characters to work with. Very good...let's move on.\n\n "];

    //step4
    [detaillist addObject:@"Taxable: There are only 2 options here! Y or N....I wonder what those could stand for? Check with your State, CFO, Boss, to determine if an item is Taxable (Y) or Non-Taxable (N)...Moving On.\n\nBEFORE WE GO FURTHER...PLEASE READ!\n MINIMUM STARTING BID:\n It is important to remember that THIS IS AN AUCTION...the Minimum Starting Bid is just that: A PLACE TO GET STARTED. We start almost every item at $1 (whether it's worth $1 or $100,000.)\n Nothing is more irritating to an online buyer than an unreasonable starting bid. PLEASE don't worry...if your item has value, buyers will fight each other until fair market value has been determined. Truthfully, you and I really don't determine the value of an item. The BUYER, through competitive bidding, in an OPEN MARKET, does...ALWAYS! If you are bent on putting retail prices in the minimum starting bid, you may want to delete this app, and forget the entire auction concept. Auctions work, but you must let the buyer engage the item! So....\n\nPut 1 (no dollar signs) in the minimum starting bid. Now let's say you have an item (lot) with 100 lunch trays and you will be selling them \"By the Piece\" x 100. Well...a $1 minimum starting bid is too high. Perhaps try starting at .10 (10 cents)...\n\n RESERVE PRICE:\nAt Bid-Assets we like to run NO RESERVE auctions so please leave this field blank. Our opinion is that if you want to sell it SELL IT you should do this... If not, keep it. Again, this is all about selling your items to the highest bidder for the HIGHEST PRICE! With an un-met reserve, you get NO MONEY, and you STILL have the item that you wanted to sell in the first place. So again, PLEASE leave this field blank to ensure that your item sells!\n\n"];

    //step5
    [detaillist addObject:@"Ok, this is VERY important. Items are sold in LOTS (as in a group of items) or BY THE PIECE (meaning item by item). If you are selling a single item, or a group of items together (such as on a pallet), enter 1 in the quantity field. In our example with the 100 lunch trays, you can sell them by the piece (TIMES 100) by entering 100 in the quantity. The buyer must take them all. If you want smaller groups of items, divide the trays in to groups of 50 and sell them by the piece (TIMES) by entering 50 in the quantity...again, it's up to you.\n\nSELLER:\n  Think about who is getting the proceeds check! The Seller number designates who gets the funds for which items. If all proceeds are going into one account, everything will sell under Seller 1, so you would enter 1. If different checks will be sent to different departments, make sure that you designate on each item the specific Seller number. For example: Georgia Store Seller 1, Ohio Store Seller 2, and so on. Hang in there...we're almost done!\n\n BUY NOW PRICE: \n Most of the time we do not use the Buy It Now Price. The reason? Online buyers want to bid! You'll get the most money possible by allowing competitive bidding to take place, so my advice would be to leave it blank. Ok...this is my favorite part! Click Next!\n\n"];

    [detaillist addObject:@"PHOTOS (the fun part!)\nWow! Magically, your camera should have come on. What does this mean? Anybody? It's time to take pictures....but...\nBEFORE WE GO FURTHER...PLEASE LISTEN!\nMany of us are proud of our amazing smartphones and tablets...we stand around the office bragging about megapixels and screen size, but this is not the time for that. We don't need or want 25 megapixel photos, so let's humble ourselves, go into our camera settings, and set the camera to between 3 and 5 megapixels MAX! The app is going to reduce and resize them anyway, so let's stop and do this now. Change your camera settings now please.\n\nAll done? Great...finally time to take pictures: THE ABSOLUTE, MOST IMPORTANT, MOST CRUCIAL, part of the process. You've heard that a picture speaks 1000 words. It's never been more true than at an on-line auction. This is THE WAY that buyers will assess your items! The better the quality and detail of your photos, the more money your items will bring...PERIOD!\n\nThat's why we put so much thought into the photo taking ability and simple photo tools. Go ahead and take your first picture. Steady...steady...don't move...now, CLICK! Hey, looks pretty good (or does it?) ...If you like it, go ahead and save (or click checkmark on some phones). If it is anything but perfect, click Discard or (X) and take a better one. Once you have one that you like, you can crop your photo to remove all of the background \"Noise\". Remember you're wanting to draw attention to the item...not it's surroundings! Chances are, your items will be piled up all around and you'll be using this crop feature A LOT! Oh, and another thing...Is your photo right-side up or sideways? Want to know how it will look online for sure? Go back to the listing page and click on the BLACK CAMERA w/ edit pencil. Did you click it? That is how your photo will look online. Don't like it? Click the trash can and try again by pressing the \"Clear\" camera icon. When you are satisfied with your results....Press SAVE.\n\n The order that you take your pictures in is how they will show up online. Your FIRST PHOTO is your MAIN PHOTO that everyone will see first. The others will be viewed only AFTER the online viewer clicks onto the first one. So make your FIRST PHOTO COUNT! Did I say that you can take up to 10 PHOTOS? Some items are worthy...some are not. You decide! I'm not just talking about the \"Pretty\" sides! Show everything, BARE IT ALL...the good, the bad, and the ugly! The buyer NEEDS to see this!\n\n PHOTOS/THUMBNAIL:\nDid you notice the little miracle that just happened? When you took your photos, List Assist, out of the goodness of it's heart, renamed them in the proper format. It did this just for you because it knows how badly you would hate to go back and have to rename all of the files to match each item. Do you know what else it did for you? It resized your images to the proper online format for the Bid-Assets.com auc-tion portal. I'll give you a moment to wipe the tears of joy from your eyes.Ok... pull yourself together! I know, I know...List Assist loves you too.\n\n OK, PRESS SAVE! YOU'VE JUST ENTERED YOUR FIRST ITEM, PICTURES AND ALL! You and your team members will do this process over, and over again until all items have been recorded.\n\nCOPY SIMILAR ITEMS:\nYou may have 50 similar items that you want to give a separate item number, BUT you don't want to reenter all of the data and take new pictures for each one. EASY. See at the top of the Add Item screen where it says \"Copy Item?\" Enter the Item number that you want to copy here. Then, press COPY ITEM. All fields and photos have been copied EXACTLY. If anything about the item is different such as quantities, pictures, etc...you can still make changes and then press save.\n\n"];


    [detaillist addObject:@"Let's say you have an aspect of an item that you want to copy and paste, or just clear that field. EASY...Just hold down on that field for a second and that will bring up a list to Copy, Paste, or Clear! If you want to paste this information into any other Item, then just hold down on the blank area of the new item's field and select Paste.\n\n"];

    [detaillist addObject:@"You want to get to the LIST VIEW of the items that you enter (not the Add an Item Screen). Scroll to the item that you want to Delete. Hold down on the item that you want to delete, and then simply select Delete. If you've used this particular item to \"Copy,\" don't worry. Your photos from this items still remain intact to the copied items.\n"];

    [detaillist addObject:@"If you are to this point, ALL of your items have been entered, carefully reviewed, and are now ready to export. If this is not the case, PLEASE go back and make any corrections. This is how your items will now appear online. Ok...if you are sure of your results, go ahead and click EXPORT at the bottom of the screen. WHAT'S THIS?! A password?! If you are contracted with Bid-Assets, then we will provide you with a password. The use of this app may ONLY be used with Bid-Assets and NO OTHER auction platform, as per the terms and agreements! I'm assuming that you are contracted so please enter your password and click \"Export\". Even after you do this, if you need to add items, make changes, etc., you can \"re-export\" and your new data will \"overwrite\" the old.\n\nBEFORE WE GO FURTHER...PLEASE LISTEN!\nThis is the only time where you will need to have a wifi connection. With 100s or 1000s of photos, this can take a while, so make sure that your device is plugged in or hooked up to a battery bank. Also make sure that the settings of your phone doesn't allow it to \"sleep\".\nTime to upload: Press \"Upload\" and you will be asked if you want to upload photos with the csv file. If this is your initial upload, you will probably want to upload the photos as well. \nLet's say that you've already uploaded your csv file and the photos, but you've just made some changes to your data (changed quantities, descriptions, seller number, etc.) If there are no new photos, and you just want to send the \"Corrected\" csv file, click \"No\" to hotos.\n\n"];


    [detaillist addObject:@"You may store and work on multiple auctions on your phone. Each time that you create a \"New\" auction, List-Assist separates it from the others. Just be certain that you enter your data into the CORRECT auction. \nWhen you upload, make sure that you select the correct auction.\n\n"];

    [detaillist addObject:@"Make sure the name of the auction is EXACTLY the same on each device. Don't assume! Check All Devices Now. ALWAYS use a different item number for each item. When working as a team, MAKE SURE item numbers DO NOT overlap between phones! Remember to hit \"Save\" after each item. Come up with a Category list within your team so similar items are in the same category. Give a PRECISE, organized description. Manufacturer, Model, Description, Condition...Whatever order you want, as long as order is the same on all phones/tablets.\nTake EXCEPTIONAL pictures. Crop them nicely. Show the good and the bad! In order to Export your items, you'll need a Password. You'll have this when you contract with Bid-Assets.\n\nCONTACT Bid-Assets\nWe are here to help. If these instructions are not precise enough, and you need a caring and friendly voice to help guide you, please either:\n\nCALL US: 330-219-8855 for assistance\nEMAIL US: auctions@bid-assets.com \n"];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count] + (expandedRowIndex != -1 ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    NSInteger dataIndex = [self dataIndexForRowIndex:row];
    NSString *dataObject = [data objectAtIndex:dataIndex];
    
    BOOL expandedCell = expandedRowIndex != -1 && expandedRowIndex + 1 == row;
    
    if (!expandedCell)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"data"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"data"];

        [[cell textLabel] setNumberOfLines:0]; // unlimited number of lines
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        [[cell textLabel] setFont:[UIFont systemFontOfSize: 15.0]];
        
        cell.textLabel.text = dataObject;
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expanded"];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"expanded"];

        [[cell detailTextLabel] setNumberOfLines:0]; // unlimited number of lines
        [[cell detailTextLabel] setLineBreakMode:UILineBreakModeWordWrap];
        [[cell detailTextLabel] setFont:[UIFont systemFontOfSize: 12.0]];

        cell.detailTextLabel.text = (NSString *)[detaillist objectAtIndex:row - 1];
        return cell;
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;
    
    if (row == expandedRowIndex + 1 && expandedRowIndex != -1)
        return nil;
    
    [tableView beginUpdates];
    
    if (expandedRowIndex != -1)
    {
        NSInteger rowToRemove = expandedRowIndex + 1;
        
        preventReopen = row == expandedRowIndex;
        if (row > expandedRowIndex)
            row--;
        expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        
    }
    [tableView endUpdates];
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (expandedRowIndex != -1 && row == expandedRowIndex + 1) {
        NSString *cellText = (NSString *)[detaillist objectAtIndex:row - 1];
        UIFont *cellFont = [UIFont fontWithName:@"Helvetica" size:12.0];
        CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
        CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        
        return labelSize.height + 20;
    }
    return 80;
}

- (NSInteger)dataIndexForRowIndex:(NSInteger)row
{
    if (expandedRowIndex != -1 && expandedRowIndex <= row)
    {
        if (expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}

- (IBAction)OnBack:(id)sender {
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
//    if (self.delegate)
//        [self.delegate OnCancel:self];
//    else
    [self.navigationController popViewControllerAnimated:YES];

}
@end
