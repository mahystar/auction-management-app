//
//  WorkSheetAddViewController.m
//  List Assist
//
//  Created by Admin on 9/15/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "WorkSheetAddViewController.h"
#import "AppDelegate.h"
#import "Common.h"

@interface WorkSheetAddViewController ()

@end

@implementation WorkSheetAddViewController

@synthesize txtTitle, txtNote;
@synthesize btnSave, btnClose;

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)checkUniqueTitle
{
    NSMutableArray *sheetlist;
    
    if (sheetlist)
        sheetlist=nil;
    sheetlist=[[NSMutableArray alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Sheet"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    for(int i = 0;i<[fetchedObjects count];i++)
    {
        Sheet *obj = (Sheet *)[fetchedObjects objectAtIndex:i];
        if ([obj.sheetname isEqualToString:txtTitle.text]) {
            return false;
        }
    }
    
    return true;
}


- (IBAction)OnSave:(id)sender {

    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
    BOOL flg = [self checkUniqueTitle];
    if (flg == false)
    {
        [Common showToastWithMessage:@"Title is duplicated, please try again." parent:self];
        return;
    }

    // Save
    Sheet*  newSheet =[NSEntityDescription
                         insertNewObjectForEntityForName:@"Sheet"
                         inManagedObjectContext:[self appDelegate].sheetDataContext];
    newSheet.sheetname = txtTitle.text;
    newSheet.sheetdesc = txtNote.text;

    NSError *error = nil;
    if (![[self appDelegate].sheetDataContext save:&error]) {
        //handle your error
    }
    
    if (self.delegate)
        [self.delegate OnSave:self];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)OnExit:(id)sender {
    
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }

    if (self.delegate)
        [self.delegate OnCancel:self];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)OnHelp:(id)sender {
    UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
    HelpViewController *wsAddVC = (HelpViewController *)[board instantiateViewControllerWithIdentifier:@"HelpList"];
    [self.navigationController pushViewController:wsAddVC animated:YES];
}

# pragma mark Gesture selector
- (void)backgroundTap:(UITapGestureRecognizer *)backgroundTap {
    if(currentResponder){
        [currentResponder resignFirstResponder];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    currentResponder = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    currentResponder = nil;
}

#pragma mark Keyboard Methods

- (void)keyboardShowing:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardHiding:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}
@end
