//
//  AppDelegate.m
//  List Assist
//
//  Created by Admin on 9/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    _sheetDataContext = [self appContext];
    
    
    // The iOS device = iPhone or iPod Touch
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    CGRect scrRect = CGRectMake(0, 0, iOSDeviceScreenSize.width, iOSDeviceScreenSize.height);
    
    UIStoryboard *iPhoneStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // Instantiate the initial view controller object from the storyboard
    UIViewController *initialViewController = [iPhoneStoryboard instantiateInitialViewController];
    
    // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = [[UIWindow alloc] initWithFrame:scrRect];
    
    // Set the initial view controller to be the root view controller of the window object
    self.window.rootViewController  = initialViewController;
    
    // Set the window object to be the key window and show it
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self saveContext];
}

#pragma mark - Core Data stack
- (NSManagedObjectContext *)appContext
{
    if (self.sheetDataContext != nil) {
        return self.sheetDataContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self appStoreCoordinator];
    if (coordinator != nil) {
        _sheetDataContext = [[NSManagedObjectContext alloc] init];
        [_sheetDataContext setUndoManager:nil];
        [_sheetDataContext setPersistentStoreCoordinator:coordinator];
        
    }
    
    return _sheetDataContext;
}
- (NSManagedObjectModel *)appModel
{
    if (self.sheetDataModel != nil) {
        return self.sheetDataModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SheetModel" withExtension:@"momd"];
    _sheetDataModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return self.sheetDataModel;
}

- (NSPersistentStoreCoordinator *)appStoreCoordinator
{
    if (self.persisStoreCoodinator != nil) {
        return self.persisStoreCoodinator;
    }
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"listAssit.sqlite"];
    NSError *error = nil;
    _persisStoreCoodinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self appModel]];
    if (![self.persisStoreCoodinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        abort();
    }
    
    return self.persisStoreCoodinator;
}
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (void)saveContext
{
    NSError *error = nil;
    //NSManagedObjectContext *managedObjectContext =self.sheetDataContext;
    if (_sheetDataContext != nil) {
        if ([self.sheetDataContext  hasChanges] && ![self.sheetDataContext save:&error]) {
            
            abort();
        }
    }
}

@end
