//
//  SheetEditViewController.m
//  List Assist
//
//  Created by Admin on 9/19/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import "SheetEditViewController.h"
#import "SheetListViewController.h"

#import "Common.h"
#import "LastSheetItem.h"
#import <QuartzCore/QuartzCore.h>

@interface SheetEditViewController ()

@end

@implementation SheetEditViewController

@synthesize sheetitemlist;
@synthesize sheetTitle;
@synthesize sheetname;
@synthesize sheetitem;
@synthesize currIndex;
@synthesize photos;
@synthesize thumbnails;

@synthesize lblSheetEditTitle;
@synthesize txtItemNum;

@synthesize txtItem;
@synthesize txtCategory;
@synthesize txtDescription;
@synthesize txtAddDescription;
@synthesize txtTaxable;
@synthesize txtMSB;
@synthesize txtReservePrice;
@synthesize txtQuantity;
@synthesize txtSeller;
@synthesize txtBuyNowPrice;
@synthesize txtPhotos;
@synthesize txtThumbnail;
@synthesize txtCopyItem;
@synthesize autocompleteDescs;
@synthesize autocompleteTableView;

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.scrollViewPanel setContentSize:CGSizeMake(320, 350)];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    [self.view addGestureRecognizer:tap];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:[self appDelegate].sheetDataContext];
    
    _assetLibrary = [[ALAssetsLibrary alloc] init];
    
    [lblSheetEditTitle setText:sheetTitle];
    
    txtPhotos.enabled = NO;
    txtThumbnail.enabled = NO;
    
    txtDescription.layer.cornerRadius = 5;
    txtDescription.clipsToBounds = YES;
    txtAddDescription.layer.cornerRadius = 5;
    txtAddDescription.clipsToBounds = YES;

    self.autocompleteDescs = [[NSMutableArray alloc] init];
//    self.descriptionList = [[NSMutableArray alloc] init];
    
    autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, 320, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    [self initializeList];

    _serialQueue = dispatch_queue_create("getImageCropper", DISPATCH_QUEUE_SERIAL);
}

- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    
    if (updatedObjects.count > 0) {
        for (NSManagedObject *obj in updatedObjects.allObjects) {
            NSLog(@"Object updated: %@ with values:",obj.entity.name);
//            NSDictionary *theAttributes = [[self appDelegate] getAllAttributesOf:obj];
//            for (NSString *attributeName in theAttributes) {
//                NSLog(@"Name: %@ : %@",attributeName,[obj valueForKey:attributeName]);
//            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)initializeList
{
    [txtItem setText:sheetitem.item];
    [txtCategory setText:sheetitem.category];
    [txtDescription setText:sheetitem.desc];
    [txtAddDescription setText:sheetitem.additiondesc];
    [txtTaxable setText:sheetitem.taxable];
    [txtMSB setText:sheetitem.minstartingbid];
    [txtReservePrice setText:sheetitem.reserveprice];
    [txtQuantity setText:sheetitem.quantity];
    [txtSeller setText:sheetitem.seller];
    [txtBuyNowPrice setText:sheetitem.buynowprice];
    [txtPhotos setText:sheetitem.photos];
    [txtThumbnail setText:sheetitem.thumbnails];
    
    if (photos != nil) {
        photos = nil;
    }

    if (thumbnails != nil) {
        thumbnails = nil;
    }

    if (photos == nil) {
        photos = [[NSMutableArray alloc] init];
        if (sheetitem.photos != nil && [sheetitem.photos length] > 0) {
            [photos addObjectsFromArray:[sheetitem.photos componentsSeparatedByString:@","]];
        }
    }

    if (thumbnails == nil) {
        thumbnails = [[NSMutableArray alloc] init];
        if (sheetitem.thumbnails != nil && [sheetitem.thumbnails length] > 0) {
            [thumbnails addObjectsFromArray:[sheetitem.thumbnails componentsSeparatedByString:@","]];
        }
    }
    
    [self showPhotoName];
}

- (IBAction)OnPhoto:(id)sender
{
    if ([photos count] == 5) {
        [Common showToastWithMessage:@"There are too many photos. You have to delete it first." parent:self];
    } else {
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPicker animated:YES completion:NULL];
    }
}

- (IBAction)OnPhotoEdit:(id)sender
{
    if ([photos count] > 0)
    {
        UIStoryboard *board=[UIStoryboard storyboardWithName:[Common getMainStoryBoardName] bundle:nil];
        
        PhotoEditViewController *photoeditVC = (PhotoEditViewController *)[board instantiateViewControllerWithIdentifier:@"PhotoEdit"];
        photoeditVC.delegate = self;
        photoeditVC.photos = photos;
        photoeditVC.thumbnails = thumbnails;
        photoeditVC.sheetname = sheetname;
        
        [self.navigationController pushViewController:photoeditVC animated:YES];
    }
}

- (IBAction)OnDelete:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                    message:@"Are you sure to delete all photos?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            [self deleteAllPhotos];
            break;
    }
}

-(void) deleteAllPhotos
{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    docDir = [docDir stringByAppendingPathComponent:sheetname];
    
    int i= 0;
    for (NSString *photo in photos) {
        NSString *fpath = [docDir stringByAppendingPathComponent:photo];
        [self removeImage:fpath];

        NSString *tname = [thumbnails objectAtIndex:i];
        [self removeImage:[docDir stringByAppendingPathComponent:tname]];
        
        i++;
    }

    [photos removeAllObjects];
    [thumbnails removeAllObjects];
    [self showPhotoName];
}

-(SheetItem *)getSheetItemFromID:(NSString *)sheetName currentId:(NSString *) itemId
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"sheetname == %@ AND item == %@", sheetName, itemId];
    [fetchRequest setPredicate:pred2];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count] > 0) {
        return [fetchedObjects objectAtIndex:0];
    }
    
    return nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    
    return NO; // We do not want UITextField to insert line-breaks.
    
//    [textField resignFirstResponder];
//    return YES;
    
}

- (IBAction)OnCopyItem:(id)sender {
    
    SheetItem *copyitem = [self getSheetItemFromID:sheetname currentId:txtCopyItem.text];
    
    if (copyitem != nil) {
        txtCategory.text = copyitem.category;
        txtDescription.text = copyitem.desc;
        txtAddDescription.text = copyitem.additiondesc;
        txtTaxable.text = copyitem.taxable;
        txtMSB.text = copyitem.minstartingbid;
        txtReservePrice.text = copyitem.reserveprice;
        txtQuantity.text = copyitem.quantity;
        txtSeller.text = copyitem.seller;
        txtBuyNowPrice.text = copyitem.buynowprice;
    }
}

- (IBAction)OnPrev:(id)sender {
    if ([currIndex intValue] < [sheetitemlist count] - 1) {
        self.sheetitem = [sheetitemlist objectAtIndex:[currIndex intValue] + 1];
        int targetnum = [currIndex intValue] + 1;
        currIndex = [NSNumber numberWithInt: targetnum];
        [self initializeList];
    }
}

- (IBAction)OnNew:(id)sender {
    NSString *currSheetName = sheetitem.sheetname;
     
    /* --- Add by Zhongyu ---
     Get the last sheet item and set is as newe item id.
     */
    SheetItem*  newitem =[NSEntityDescription
                          insertNewObjectForEntityForName:@"SheetItem"
                          inManagedObjectContext:[self appDelegate].sheetDataContext];
    
    NSString *newid = [self.listViewController GetNewItemID:currSheetName];
    newitem.item = newid;
    
    SheetItem *iteminfo = [self.listViewController getLastSheetItemFromID:currSheetName currentId:newid];
    if (iteminfo != nil) {
        newitem.category = iteminfo.category;
        newitem.taxable = iteminfo.taxable;
        newitem.minstartingbid = iteminfo.minstartingbid;
        newitem.quantity = iteminfo.quantity;
        newitem.seller = iteminfo.seller;
        newitem.buynowprice = iteminfo.buynowprice;
    }
    
    self.sheetTitle = @"Add Item";
    self.sheetitem = newitem;
//    self.sheetname = sheetname;
    [self initializeList];
    
}

//Add by ZhongYu
- (BOOL)checkRequiredData
{
    BOOL rst = true;
    
    if ([txtItem.text length] == 0 || [txtCategory.text length] == 0 || [txtDescription.text length] == 0 ||
        [txtTaxable.text length] == 0 || [txtMSB.text length] == 0 || [txtQuantity.text length] == 0 || [txtSeller.text length] == 0) {
        rst = false;
    }
    
    return rst;
}

- (BOOL) checkUniqueItem
{
    BOOL rst = true;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sheetname == %@ AND item == %@", sheetname, txtItem.text];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count] > 0) {
        NSString *temptitle = [NSString stringWithFormat:@"%@", sheetTitle];
        NSRange range = [temptitle rangeOfString:@"Edit"];
        if (range.location != NSNotFound) {
//        if ([sheetTitle containsString:@"Edit"]) {
            SheetItem *sinfo = (SheetItem *)[fetchedObjects objectAtIndex:0];
            if (![sinfo.item isEqualToString:sheetitem.item]) {
                rst = false;
            }
        } else {
            rst = false;
        }
    }
    
    return rst;
}

- (LastSheetItem *)GetLastSheetItem
{
    LastSheetItem *lastitem = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LastSheetItem"
                                              inManagedObjectContext:[self appDelegate].sheetDataContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].sheetDataContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count] > 0) {
        lastitem = (LastSheetItem *)[fetchedObjects objectAtIndex:0];
    }
    
    return lastitem;
}

- (void)SaveItemInfo
{
    NSString *temptitle = [NSString stringWithFormat:@"%@", sheetTitle];
    NSRange range = [temptitle rangeOfString:@"Add"];
    if (range.location != NSNotFound)
//    if ([temptitle containsString:@"Add"])
    {
        SheetItem*  newitem =[NSEntityDescription
                              insertNewObjectForEntityForName:@"SheetItem"
                              inManagedObjectContext:[self appDelegate].sheetDataContext];
        
        newitem.sheetname = sheetname;
        newitem.item = txtItem.text;
        newitem.desc = txtDescription.text;
        newitem.category = txtCategory.text;
        newitem.additiondesc = txtAddDescription.text;
        newitem.taxable = txtTaxable.text;
        newitem.minstartingbid = txtMSB.text;
        newitem.reserveprice = txtReservePrice.text;
        newitem.quantity = txtQuantity.text;
        newitem.seller = txtSeller.text;
        newitem.buynowprice = txtBuyNowPrice.text;
        
        newitem.photos = [photos componentsJoinedByString:@","];
        newitem.thumbnails = [thumbnails componentsJoinedByString:@","];
    } else {
        sheetitem.item = txtItem.text;
        sheetitem.desc = txtDescription.text;
        sheetitem.category = txtCategory.text;
        sheetitem.additiondesc = txtAddDescription.text;
        sheetitem.taxable = txtTaxable.text;
        sheetitem.minstartingbid = txtMSB.text;
        sheetitem.reserveprice = txtReservePrice.text;
        sheetitem.quantity = txtQuantity.text;
        sheetitem.seller = txtSeller.text;
        sheetitem.buynowprice = txtBuyNowPrice.text;
        sheetitem.photos = [photos componentsJoinedByString:@","];
        sheetitem.thumbnails = [thumbnails componentsJoinedByString:@","];
    }
    
    LastSheetItem *lastitem = [self GetLastSheetItem];
    
    if (lastitem == nil)
    {
        LastSheetItem*  newitem =[NSEntityDescription
                              insertNewObjectForEntityForName:@"LastSheetItem"
                              inManagedObjectContext:[self appDelegate].sheetDataContext];
        
        newitem.item = txtItem.text;
        newitem.sheetname = sheetname;
    } else {
        lastitem.item = txtItem.text;
    }

    NSError *error = nil;
    if (![[self appDelegate].sheetDataContext save:&error]) {
        
        //handle your error
    }
    
}

- (IBAction)OnSave:(id)sender {

    // check unique item id
    if (![self checkUniqueItem]) {
        NSString *msg = @"Item is already existing, please check again.";
        [Common showToastWithMessage:msg parent:self];
        return;
    }
    
    // check empty fields to save
    if (![self checkRequiredData]) {
        NSString *msg = @"Please input ITEM, CATEGORY, DESCRIPTION, TAXABLE, MIN STARTING BID, QUANTITY, SELLER first.";
        [Common showToastWithMessage:msg parent:self];
        return;
    }
    
    // Save
    [self SaveItemInfo];
    
    if (self.delegate)
        [self.delegate OnSheetEditSave:self];
    else
        [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)OnClose:(id)sender {
    
    if (self.delegate)
        [self.delegate OnSheetEditClose:self];
    else
        [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)OnLast:(id)sender {
    if ([currIndex intValue] > 0) {
        self.sheetitem = [sheetitemlist objectAtIndex:[currIndex intValue] - 1];
        int targetnum = [currIndex intValue] - 1;
        currIndex = [NSNumber numberWithInt: targetnum];
        [self initializeList];
    }
}

#pragma mark - UIImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissViewControllerAnimated:NO completion:^{
       
        ImageCropViewController *cropVC = [[ImageCropViewController alloc] initWithImage:image];
        cropVC.delegate = self;
        [self presentViewController:cropVC animated:YES completion:NULL];
        
    }];
}

- (void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [self dismissViewControllerAnimated:NO completion:^{
        
        int PMAX_WIDTH = 720;
        int TMAX_WIDTH = 240;
        
        CGFloat scaleRate = 0.0, scaleRateT = 0.0;
        
        scaleRate = croppedImage.size.width / croppedImage.size.height;
        
//        if (scaleRate >= 1) {
            scaleRate = PMAX_WIDTH / croppedImage.size.width;
            scaleRateT = TMAX_WIDTH / croppedImage.size.width;
//        } else {
//            scaleRate = PMAX_WIDTH / croppedImage.size.height;
//            scaleRateT = TMAX_WIDTH / croppedImage.size.height;
//        }
        
        CGFloat targetW = scaleRate * croppedImage.size.width;
        CGFloat targetH = scaleRate * croppedImage.size.height;
        
        CGFloat thumbW = scaleRateT * croppedImage.size.width;
        CGFloat thumbH = scaleRateT * croppedImage.size.height;
        
        // Main Image Processing...
        CGSize finalSize = CGSizeMake(targetW, targetH);
        
        UIGraphicsBeginImageContext(finalSize);
        
        //Fetch First image and draw in rect
        CGRect rect = CGRectMake(0, 0, finalSize.width, finalSize.height);
        [croppedImage drawInRect:rect];
        
        //Fetch second image and draw in rect
        UIImage *markImg = [UIImage imageNamed:@"Logo720"];
        CGSize markSize = [markImg size];
        CGRect markRect = CGRectMake(0, finalSize.height - markSize.height, markSize.width, markSize.height);
        [markImg drawInRect:markRect];
        
        
        UIImage* finalImg = UIGraphicsGetImageFromCurrentImageContext();// it will  returns an image based on the contents of the current bitmap-based graphics context.
        
        if (finalImg != nil)
        {
            // Save to the default
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            docDir = [docDir stringByAppendingPathComponent:sheetname];
            
            NSError *error = [[NSError alloc] init];
            if (![[NSFileManager defaultManager] fileExistsAtPath:docDir])
                [[NSFileManager defaultManager] createDirectoryAtPath:docDir withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
            
            // Append the Image filename ID(ex: A, B, C, D, E, ...)
            NSString *fpath = nil;
            NSString *fname = [txtItem.text stringByAppendingPathExtension:@"jpg"];
            NSString *tname = [[txtItem.text stringByAppendingString:@"t"] stringByAppendingPathExtension:@"jpg"];

            if (![photos containsObject:fname])
            {
                fpath = [docDir stringByAppendingPathComponent:fname];
                BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
                if (aExist)
                {
                    [self removeImage:fpath];
                    [self removeImage:[docDir stringByAppendingPathComponent:tname]];
                }
            } else {
                fname = [[txtItem.text stringByAppendingString:@"A"] stringByAppendingPathExtension:@"jpg"];
                tname = [[txtItem.text stringByAppendingString:@"At"] stringByAppendingPathExtension:@"jpg"];
                
                if (![photos containsObject:fname])
                {
                    fpath = [docDir stringByAppendingPathComponent:fname];
                    BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
                    if (aExist)
                    {
                        [self removeImage:fpath];
                        [self removeImage:[docDir stringByAppendingPathComponent:tname]];
                    }
                } else {
                    fname = [[txtItem.text stringByAppendingString:@"B"] stringByAppendingPathExtension:@"jpg"];
                    tname = [[txtItem.text stringByAppendingString:@"Bt"] stringByAppendingPathExtension:@"jpg"];
                    
                    if (![photos containsObject:fname])
                    {
                        fpath = [docDir stringByAppendingPathComponent:fname];
                        BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
                        if (aExist)
                        {
                            [self removeImage:fpath];
                            [self removeImage:[docDir stringByAppendingPathComponent:tname]];
                        }
                    } else {
                        fname = [[txtItem.text stringByAppendingString:@"C"] stringByAppendingPathExtension:@"jpg"];
                        tname = [[txtItem.text stringByAppendingString:@"Ct"] stringByAppendingPathExtension:@"jpg"];
                        
                        if (![photos containsObject:fname])
                        {
                            fpath = [docDir stringByAppendingPathComponent:fname];
                            BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
                            if (aExist)
                            {
                                [self removeImage:fpath];
                                [self removeImage:[docDir stringByAppendingPathComponent:tname]];
                            }
                        } else {
                            fname = [[txtItem.text stringByAppendingString:@"D"] stringByAppendingPathExtension:@"jpg"];
                            tname = [[txtItem.text stringByAppendingString:@"Dt"] stringByAppendingPathExtension:@"jpg"];
                            
                            if (![photos containsObject:fname])
                            {
                                fpath = [docDir stringByAppendingPathComponent:fname];
                                BOOL aExist = [[NSFileManager defaultManager] fileExistsAtPath:fpath];
                                if (aExist)
                                {
                                    [self removeImage:fpath];
                                    [self removeImage:[docDir stringByAppendingPathComponent:tname]];
                                }
                            }
                        }
                    }
                }
            }
            
            [photos addObject:fname];
            
            NSData *photoData = [NSData dataWithData:UIImageJPEGRepresentation(finalImg, 80)];
            [photoData writeToFile:[docDir stringByAppendingPathComponent:fname] atomically:YES];
            
            // Save to the camera roll
            UIImageWriteToSavedPhotosAlbum(finalImg, nil, nil, nil);
            
            CGSize finalSizeT = CGSizeMake(thumbW, thumbH);
            
            UIGraphicsBeginImageContext(finalSizeT);
            
            //Fetch First image and draw in rect
            CGRect rectT = CGRectMake(0, 0, finalSizeT.width, finalSizeT.height);
            [croppedImage drawInRect:rectT];
            
            //Fetch second image and draw in rect
            UIImage *markImgT = [UIImage imageNamed:@"Logo240"];
            CGSize markSizeT = [markImgT size];
            CGRect markRectT = CGRectMake(0, finalSizeT.height - markSizeT.height, markSizeT.width, markSizeT.height);
            [markImgT drawInRect:markRectT];
            
            
            UIImage* finalImgT = UIGraphicsGetImageFromCurrentImageContext();// it will  returns an image based on the contents of the current bitmap-based graphics context.
            
            if (finalImgT != nil)
            {
                NSString *thumbnamepath = [docDir stringByAppendingPathComponent:tname];
                
                NSData *photoDataT = [NSData dataWithData:UIImageJPEGRepresentation(finalImgT, 80)];
                [photoDataT writeToFile:thumbnamepath atomically:YES];
                
                [thumbnails addObject:tname];
                
                // Save to the camera roll
                UIImageWriteToSavedPhotosAlbum(finalImgT, nil, nil, nil);
            }
            
            UIGraphicsEndImageContext();

        }
        
        UIGraphicsEndImageContext();
        
        [self showPhotoName];
    }];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (BOOL)removeImage:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        return true;
    }
    else
    {
        return false;
    }
}

-(void) showPhotoName
{
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];

    NSString *tname = [[txtItem.text stringByAppendingString:@"t"] stringByAppendingPathExtension:@"jpg"];

    [photos sortUsingDescriptors:sortDescriptors];
    [thumbnails sortUsingDescriptors:sortDescriptors];
    
    if ([thumbnails containsObject:tname])
    {
        NSString *tmpname = [[NSString alloc] initWithString:[thumbnails objectAtIndex:0]];
        
        if (![tmpname isEqualToString:tname]) {
            int tInd = [thumbnails indexOfObject:tname];
//            [thumbnails removeObjectAtIndex:0];
            [thumbnails removeObjectAtIndex:tInd];
            [thumbnails insertObject:tname atIndex:0];
//            [thumbnails replaceObjectAtIndex:tInd withObject:tmpname];
        }
    }
    
    txtPhotos.text = [photos componentsJoinedByString:@","];
    txtThumbnail.text = [thumbnails componentsJoinedByString:@","];
}

- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller
{
    [self dismissViewControllerAnimated:NO completion:^{}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];    
}

#pragma mark - PhotoEditViewController delegate methods
- (void)OnPhotoEditClose:(PhotoEditViewController *)photoeditVC
{
    sheetitem.photos = [photoeditVC.photos componentsJoinedByString:@","];
    sheetitem.thumbnails = [photoeditVC.thumbnails componentsJoinedByString:@","];
    [photoeditVC.navigationController popViewControllerAnimated:YES];
    
    // Get sheet item again
    
    [self initializeList];
}

# pragma mark Gesture selector
- (void)backgroundTap:(UITapGestureRecognizer *)backgroundTap {
    if(currentResponder){
        [currentResponder resignFirstResponder];
        [self.view layoutIfNeeded];
    }
    
    [self.scrollViewPanel setContentSize:CGSizeMake(320, 550)];
//    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    currentResponder = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
//    currentResponder = nil;
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [autocompleteDescs removeAllObjects];
    for(SheetItem *item in sheetitemlist) {
        NSString *curStr = [NSString stringWithString:[item description]];
        NSRange substringRange = [curStr rangeOfString:substring];
        if (substringRange.location == 0) {
            [autocompleteDescs addObject: curStr];
        }
        [autocompleteDescs addObject:@"SSS"];
    }
    [autocompleteTableView reloadData];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    autocompleteTableView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y + 60, 170, 120);
    autocompleteTableView.hidden = NO;
    
    NSString *substring = [NSString stringWithString:textView.text];
    //substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchAutocompleteEntriesWithSubstring:substring];
    return YES;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}


-(void)textViewDidBeginEditing:(UITextView *)textView {
    currentResponder = textView;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if (currentResponder) {
        [currentResponder resignFirstResponder];
    }
    
    autocompleteTableView.hidden = YES;
}

#pragma mark Keyboard Methods

- (void)keyboardShowing:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardHiding:(NSNotification *)note
{
    NSNumber *duration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    
    [UIView animateWithDuration:duration.floatValue animations:^{
        
        [self.view layoutIfNeeded];
    }];
}


#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return autocompleteDescs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    cell.textLabel.text = [autocompleteDescs objectAtIndex:indexPath.row];
    cell.textLabel.font = [cell.textLabel.font fontWithSize:10];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    txtDescription.text = selectedCell.textLabel.text;
}
@end
