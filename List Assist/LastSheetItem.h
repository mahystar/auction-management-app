//
//  LastSheetItem.h
//  List Assist
//
//  Created by Admin on 11/23/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LastSheetItem : NSManagedObject

@property (nonatomic, retain) NSString * item;
@property (nonatomic, retain) NSString * sheetname;

@end
