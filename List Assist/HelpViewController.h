//
//  HelpViewController.h
//  List Assist
//
//  Created by Admin on 11/23/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HelpViewController;

@protocol HelpViewDelegate

//- (void)OnSave:(WorkSheetAddViewController *)worksheetaddVC;
//- (void)OnCancel:(WorkSheetAddViewController *)worksheetaddVC;

@end

@interface HelpViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UIResponder *currentResponder;

    NSMutableArray *data;
    NSInteger expandedRowIndex;
}
- (IBAction)OnBack:(id)sender;

@property (nonatomic, retain) id<HelpViewDelegate> delegate;
@property (nonatomic, retain) NSMutableArray * detaillist;

@end