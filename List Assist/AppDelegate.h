//
//  AppDelegate.h
//  List Assist
//
//  Created by Admin on 9/5/14.
//  Copyright (c) 2014 BidAssets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *sheetDataContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *sheetDataModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persisStoreCoodinator;

- (NSURL *)applicationDocumentsDirectory;

@end
